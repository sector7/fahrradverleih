package de.met.bikes.offer;

import java.io.Serializable;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import de.met.bikes.action.Rental;
import de.met.bikes.action.Reservation;
import de.met.bikes.commons.Price;
import de.met.bikes.exceptions.MethodConstraintException;

/**
 * Klasse zur Darstellung eines ausleihbaren Produktes.
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Product implements Serializable {

    private static final long serialVersionUID = -7602772804668956248L;

    /** Referenz-ID */
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;

    /** Preis des Produktes */
    @Embedded
    protected Price price;

    /**
     * Erstellt ein ausleihbares Produkt.
     * 
     * @param price Ausleihpreis
     */
    public Product(Price price) {
        this.price = price;
    }

    /**
     * Leerer Konstruktor für Hibernate.
     * 
     * @deprecated nur für Hinbernate
     */
    @Deprecated
    protected Product() {
    }

    /**
     * Führt einen Ausleih-Vorgang durch.
     * 
     * @param rental Ausleihe
     * @throws MethodConstraintException
     */
    public abstract void rent(Rental rental) throws MethodConstraintException;

    /**
     * Reserviert einen Artikel.
     * 
     * @throws MethodConstraintException
     */
    public abstract void book(Reservation reservation) throws MethodConstraintException;

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

}
