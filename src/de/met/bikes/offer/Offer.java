package de.met.bikes.offer;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import de.met.bikes.action.Rental;
import de.met.bikes.action.Reservation;
import de.met.bikes.commons.Price;
import de.met.bikes.exceptions.MethodConstraintException;
import de.met.bikes.inventory.Item;

/**
 * Bildet ein Angebot ab, das mehrere Leihartikel bündelt.
 */
@Entity
public class Offer extends Product {

    private static final long serialVersionUID = -7462326657923175860L;

    /** Liste aller enthaltenen Leihartikel */
    @OneToMany(cascade = CascadeType.ALL)
    private List<Item> offerItems;

    /**
     * Initialisiert ein Angebot mit einer Liste von Leihartikeln.
     *
     * @param items Liste der Leihartikel
     */
    public Offer(final Price price, final List<Item> items) {
        super(price);
        this.offerItems = items;
    }

    /**
     * Leerer Konstruktor für Hibernate.
     *
     * @deprecated nur für Hibernate
     */
    @Deprecated
    protected Offer() {
    }

    public List<Item> getOfferItems() {
        return this.offerItems;
    }

    public void setOfferItems(final List<Item> offerItems) {
        this.offerItems = offerItems;
    }

    @Override
    public void rent(final Rental rental) throws MethodConstraintException {
        for (final Item item : this.offerItems) {
            item.rent(rental);
        }
    }

    @Override
    public void book(final Reservation reservation) throws MethodConstraintException {
        for (final Item item : this.offerItems) {
            item.book(reservation);
        }
    }

    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append("Offer mit Produkten:\n");
        for (final Item item : this.offerItems) {
            buffer.append("    ").append(item.toString()).append("\n");
        }
        buffer.append("\t\t\t-----------------");
        buffer.append("\n  ").append(this.price.toString());
        return buffer.toString();
    }
}
