package de.met.bikes.contacts;

import static de.met.bikes.action.Reservation.MAX_UNACCOMPLISHED_RESERVATIONS;

import javax.persistence.Entity;

import de.met.bikes.commons.Address;

/**
 * Bildet einen Kunden des Fahrradverleihs ab. Um zu verhindern, dass ein Kunde ständig
 * Reservierungen tätigt ohne diese abzuholen, wird pro Kunde mitgezählt, wie viele Reservierungen
 * er bereits nicht abgeholt hat.
 */
@Entity
public class Customer extends Contact {

    private static final long serialVersionUID = -8252553208254795303L;

    /** Anzahl der unerfüllten Reservierungen */
    private int unaccomplishedReservations = 0;

    @SuppressWarnings({ "unused", "deprecation" })
    private Customer() {
        super();
    }

    /**
     * Erstellt einen neuen Kunden.
     *
     * @param name Name des Kunden
     * @param address Adresse des Kunden
     */
    public Customer(final String name, final Address address) {
        super(name, address);
    };

    /**
     * Erstellt einen Kunden mit bereits unerfüllten Reservierungen.
     *
     * @param name Name
     * @param address Adresse
     * @param unaccomplishedReservations Anzahl der nicht abgeholten Reservierungen
     */
    public Customer(final String name, final Address address, final int unaccomplishedReservations) {
        super(name, address);
        this.unaccomplishedReservations = unaccomplishedReservations;
    }

    public int getUnaccomplishedReservations() {
        return this.unaccomplishedReservations;
    }

    /**
     * @return Angabe, ob der Kunde bereits die Maximalzahl unerfüllter Reservierungen erreicht hat.
     */
    public boolean hasReachedMaxUnaccomplishedReservations() {
        return this.unaccomplishedReservations > MAX_UNACCOMPLISHED_RESERVATIONS;
    }

    /**
     * Erhöht die nicht abgeholten Reservierungen.
     */
    public void incUnaccomplishedReservations() {
        this.unaccomplishedReservations += 1;
    }

    public void setUnaccomplishedReservations(final int unaccomplishedReservations) {
        this.unaccomplishedReservations = unaccomplishedReservations;
    }

    @Override
    public String toString() {
        return "Kunde: " + this.name;
    }

}
