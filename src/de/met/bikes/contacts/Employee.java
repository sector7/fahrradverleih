package de.met.bikes.contacts;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import de.met.bikes.commons.Address;

/**
 * Mitarbeiter des Fahrradverleihs.
 */
@Entity
public class Employee extends Contact {

    private static final long serialVersionUID = -2375085289753385457L;

    /**
     * Status eines Mitarbeiters.
     */
    public enum Status {
        /** arbeitet zur Zeit */
        ACTIVE,
        /** hat gekündigt */
        QUIT,
        /** pansioniert */
        RETIRED,
        /** gestorben */
        DEAD
    }

    /** Status eines Mitarbeiters */
    @Enumerated(EnumType.STRING)
    private Status status;

    /**
     * Erstellt einen Angestellten.
     *
     * @param name Name des Angestellten
     * @param address Adresse des Angestellten
     * @param status Status des Mitarbeites
     */
    public Employee(final String name, final Address address, final Status status) {
        super(name, address);
        this.status = status;
    }

    @SuppressWarnings("deprecation")
    protected Employee() {
    }

    public Status getEmployeeStatus() {
        return this.status;
    }

    public void setEmployeeStatus(final Status employeeStatus) {
        this.status = employeeStatus;
    }

}
