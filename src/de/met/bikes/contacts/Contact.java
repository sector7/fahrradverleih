package de.met.bikes.contacts;

import java.io.Serializable;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import org.hibernate.annotations.GenericGenerator;

import de.met.bikes.commons.Address;

/**
 * Darstellung eines Kommunikationspartners.
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Contact implements Serializable {

    private static final long serialVersionUID = 1271686132685183933L;

    /** Referenz-ID */
    @Id
    @GeneratedValue(strategy=GenerationType.TABLE)
    private long id;

    /** Name des Kontaktes */
    protected String name;

    /** Adresse des Kontaktes */
    @Embedded
    protected Address address;

    /**
     * Default-Konstruktor
     *
     * @deprecated Sollte nur für Hibernate benutzt werden.
     */
    @Deprecated
    protected Contact() {
    }

    /**
     * Erstellt einen neuen Kontakt.
     *
     * @param name Name des Kontaktes
     * @param address Adresse des Kontaktes
     */
    public Contact(final String name, final Address address) {
        this.name = name;
        this.address = address;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final Contact other = (Contact) obj;
        if (this.address == null) {
            if (other.address != null)
                return false;
        } else if (!this.address.equals(other.address))
            return false;
        if (this.id != other.id)
            return false;
        if (this.name == null) {
            if (other.name != null)
                return false;
        } else if (!this.name.equals(other.name))
            return false;
        return true;
    }

    public Address getAddress() {
        return this.address;
    }

    public long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.address == null) ? 0 : this.address.hashCode());
        result = prime * result + (int) (this.id ^ (this.id >>> 32));
        result = prime * result + ((this.name == null) ? 0 : this.name.hashCode());
        return result;
    }

    public void setAddress(final Address address) {
        this.address = address;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public void setName(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("Contact [id=%s, name=%s, address=%s]", this.id, this.name, this.address);
    }

}
