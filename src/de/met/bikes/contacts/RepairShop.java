package de.met.bikes.contacts;

import javax.persistence.Entity;

import de.met.bikes.commons.Address;

/**
 * Bildet eine Reparaturwerkstatt ab, die die Produkte des Fahrradverleihs repariert.
 */
@Entity
public class RepairShop extends Contact {

    private static final long serialVersionUID = -618024559755968301L;

    /**
     * Erstellt eine Reparaturwerkstatt.
     *
     * @param name Name der Werkstatt
     * @param address Adresse der Werkstatt
     */
    public RepairShop(final String name, final Address address) {
        super(name, address);
    }

    @SuppressWarnings("deprecation")
    protected RepairShop() {
    }

}
