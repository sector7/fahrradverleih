package de.met.bikes.commons;

import java.io.Serializable;

/**
 * Ein einfaches, generisches Paar.
 * @param <T1> Typ des ersten Teils des Paares
 * @param <T2> Typ des zweiten Teils des Paares
 */
public class Pair<T1, T2> implements Serializable{

    private static final long serialVersionUID = 2600484033139048811L;

    /** Erster Teil des Paares. */
	public final T1 first;
	
	/** Zweiter Teil des Paares. */
	public final T2 second;
	
	/**
	 * Erzeugt ein Paar.
	 * @param first Erster Teil des Paares
	 * @param second Zweiter Teil des Paares
	 */
	public Pair(T1 first, T2 second) {
		this.first = first;
		this.second = second;
	}

}
