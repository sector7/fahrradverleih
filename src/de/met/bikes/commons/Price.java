package de.met.bikes.commons;

import java.io.Serializable;

/**
 * Der Preis eines Produktes.
 */
public class Price implements Serializable {

    private static final long serialVersionUID = 1225770194787030188L;

    /** Stundenpreis */
    private double perHour;

    /** Tagespreis */
    private double perDay;

    /**
     * Konstruktur eines Preises.
     * 
     * @param perHour Stundenpreis
     * @param perDay Tagespreis
     */
    public Price(double perHour, double perDay) {
        this.perHour = perHour;
        this.perDay = perDay;
    }

    /**
     * Leerer Konstruktor für Hibernate.
     */
    protected Price() {
    }

    public double getPerHourPrice() {
        return perHour;
    }

    public void setPerHourPrice(double perHour) {
        this.perHour = perHour;
    }

    public double getPerDayPrice() {
        return perDay;
    }

    public void setPerDayPrice(double perDay) {
        this.perDay = perDay;
    }

    @Override
    public String toString() {
        return "\t\t\t" + this.perHour + "€/h " + this.perDay + "€/d";
    }

}
