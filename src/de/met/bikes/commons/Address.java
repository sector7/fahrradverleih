package de.met.bikes.commons;

import java.io.Serializable;

/**
 * Stellt eine postalische Adresse dar.
 */
public class Address implements Serializable {

    private static final long serialVersionUID = -6868179516661563870L;

    /** Straße */
    private String street;

    /** Hausnummer */
    private String houseNumber;

    /** PLZ */
    private int zipcode;

    /** Stadt */
    private String city;

    @SuppressWarnings("unused")
    private Address() {
        super();
    }

    /**
     * Legt eine Adresse an.
     *
     * @param street Straße
     * @param houseNumber Hausnummer
     * @param zipcode PLZ
     * @param city Stadt
     */
    public Address(final String street, final String houseNumber, final int zipcode, final String city) {
        super();
        this.street = street;
        this.houseNumber = houseNumber;
        this.zipcode = zipcode;
        this.city = city;
    };

    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final Address other = (Address) obj;
        if (this.city == null) {
            if (other.city != null)
                return false;
        } else if (!this.city.equals(other.city))
            return false;
        if (this.houseNumber == null) {
            if (other.houseNumber != null)
                return false;
        } else if (!this.houseNumber.equals(other.houseNumber))
            return false;
        if (this.street == null) {
            if (other.street != null)
                return false;
        } else if (!this.street.equals(other.street))
            return false;
        if (this.zipcode != other.zipcode)
            return false;
        return true;
    }

    public String getCity() {
        return this.city;
    }

    public String getHouseNumber() {
        return this.houseNumber;
    }

    public String getStreet() {
        return this.street;
    }

    public int getZipcode() {
        return this.zipcode;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.city == null) ? 0 : this.city.hashCode());
        result = prime * result + ((this.houseNumber == null) ? 0 : this.houseNumber.hashCode());
        result = prime * result + ((this.street == null) ? 0 : this.street.hashCode());
        result = prime * result + this.zipcode;
        return result;
    }

    public void setCity(final String city) {
        this.city = city;
    }

    public void setHouseNumber(final String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public void setStreet(final String street) {
        this.street = street;
    }

    public void setZipcode(final int zipcode) {
        this.zipcode = zipcode;
    }

    @Override
    public String toString() {
        return String.format("Address [street=%s, houseNumber=%s, zipcode=%s, city=%s]", this.street, this.houseNumber,
                this.zipcode, this.city);
    }

}
