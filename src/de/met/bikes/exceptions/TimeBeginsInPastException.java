package de.met.bikes.exceptions;

public class TimeBeginsInPastException extends MethodConstraintException {

    private static final long serialVersionUID = 1L;

    public TimeBeginsInPastException() {
        // TODO Auto-generated constructor stub
    }

    public TimeBeginsInPastException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        // TODO Auto-generated constructor stub
    }

    public TimeBeginsInPastException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

    public TimeBeginsInPastException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public TimeBeginsInPastException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

}
