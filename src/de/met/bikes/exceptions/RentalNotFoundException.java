package de.met.bikes.exceptions;

public class RentalNotFoundException extends MethodConstraintException {

    private static final long serialVersionUID = 1L;

    public RentalNotFoundException() {
    }

    public RentalNotFoundException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public RentalNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public RentalNotFoundException(String message) {
        super(message);
    }

    public RentalNotFoundException(Throwable cause) {
        super(cause);
    }

}
