package de.met.bikes.exceptions;

/**
 * Exception für den Fall, dass ein Ausleihartikel bereits ausgeliehen ist.
 */
public class ItemRentException extends MethodConstraintException {

    /** Serialisierungs-ID */
    private static final long serialVersionUID = -1145401508344428663L;

    public ItemRentException() {
    }

    public ItemRentException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public ItemRentException(String message, Throwable cause) {
        super(message, cause);
    }

    public ItemRentException(String message) {
        super(message);
    }

    public ItemRentException(Throwable cause) {
        super(cause);
    }

}
