package de.met.bikes.exceptions;

/**
 * Exception für den Fall, dass ein Ausleih-Artikel bereits reserviert wurde.
 */
public class ItemReservedException extends MethodConstraintException {

    /** Serialisierungs-ID */
    private static final long serialVersionUID = 2515513456740780381L;

    public ItemReservedException() {
    }

    public ItemReservedException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public ItemReservedException(String message, Throwable cause) {
        super(message, cause);
    }

    public ItemReservedException(String message) {
        super(message);
    }

    public ItemReservedException(Throwable cause) {
        super(cause);
    }

}
