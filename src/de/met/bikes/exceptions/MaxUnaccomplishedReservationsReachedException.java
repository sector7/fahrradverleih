package de.met.bikes.exceptions;

/**
 * Exception für den Fall, dass ein Kunde ein Reservierung aufgeben will, aber bereits das Maximum
 * nicht abgeholter Reservierungen erreicht hat.
 */
public class MaxUnaccomplishedReservationsReachedException extends MethodConstraintException {

    /** Serialisierungs-ID */
    private static final long serialVersionUID = -920105067443445541L;

    public MaxUnaccomplishedReservationsReachedException() {
    }

    public MaxUnaccomplishedReservationsReachedException(String message, Throwable cause,
            boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public MaxUnaccomplishedReservationsReachedException(String message, Throwable cause) {
        super(message, cause);
    }

    public MaxUnaccomplishedReservationsReachedException(String message) {
        super(message);
    }

    public MaxUnaccomplishedReservationsReachedException(Throwable cause) {
        super(cause);
    }

}
