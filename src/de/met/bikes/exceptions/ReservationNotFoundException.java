package de.met.bikes.exceptions;

public class ReservationNotFoundException extends MethodConstraintException {

    private static final long serialVersionUID = 1L;

    public ReservationNotFoundException() {
    }

    public ReservationNotFoundException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public ReservationNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public ReservationNotFoundException(String message) {
        super(message);
    }

    public ReservationNotFoundException(Throwable cause) {
        super(cause);
    }

}
