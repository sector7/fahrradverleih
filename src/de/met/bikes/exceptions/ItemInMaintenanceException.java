package de.met.bikes.exceptions;

/**
 * Exception für den Fall, dass sich ein Fahrrad in Reparatur befindet.
 */
public class ItemInMaintenanceException extends MethodConstraintException {

    /** Serialisierungs-ID */
    private static final long serialVersionUID = 2489273171087309825L;

    public ItemInMaintenanceException() {
    }

    public ItemInMaintenanceException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public ItemInMaintenanceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ItemInMaintenanceException(String message) {
        super(message);
    }

    public ItemInMaintenanceException(Throwable cause) {
        super(cause);
    }

}
