package de.met.bikes.exceptions;

/**
 * Exception für den Fall, dass ein Leihartikel ausgemustert wurde.
 */
public class ItemDischargedException extends MethodConstraintException {

    /** Serialisierungs-ID */
    private static final long serialVersionUID = 5382655195552809109L;

    public ItemDischargedException() {
    }

    public ItemDischargedException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public ItemDischargedException(String message, Throwable cause) {
        super(message, cause);
    }

    public ItemDischargedException(String message) {
        super(message);
    }

    public ItemDischargedException(Throwable cause) {
        super(cause);
    }

}
