package de.met.bikes.exceptions;

public class MethodConstraintException extends Exception {

	public MethodConstraintException() {
        super();
    }

    public MethodConstraintException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public MethodConstraintException(String message, Throwable cause) {
        super(message, cause);
    }

    public MethodConstraintException(String message) {
        super(message);
    }

    public MethodConstraintException(Throwable cause) {
        super(cause);
    }

    private static final long serialVersionUID = 1L;

}
