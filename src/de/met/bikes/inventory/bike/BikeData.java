package de.met.bikes.inventory.bike;

import java.io.Serializable;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

/**
 * Modellierung zusätzlicher Informationen zu einem Fahrrad.
 */
public class BikeData implements Serializable {
    private static final long serialVersionUID = -5799185182643368239L;
    /** Farbe des Fahrrads */
    private String color;
    /** Fahrradgröße (Felgen-ø in Zoll) */
    private int tireSize;
    /** Rahmenhöhe [cm] */
    private int frameHeight;
    /** Rahmennummer */
    private String frameNumber;
    /** Typ des Fahrrads. */
    @Enumerated(EnumType.STRING) 
    private Type type;

    /**
     * Konstruiert die Fahrrad-Daten.
     * 
     * @param color Farbe
     * @param tireSize Fahrradgröße
     * @param frameHeight Rahmenhöhe
     * @param frameNumber Rahmennummer
     * @param type Typ des Fahrrads
     */
    public BikeData(String color, int tireSize, int frameHeight, String frameNumber, Type type) {
        super();
        this.color = color;
        this.tireSize = tireSize;
        this.frameHeight = frameHeight;
        this.frameNumber = frameNumber;
        this.type = type;
    }

    /**
     * Leerer Konstruktor für Hibernate.
     * 
     * @deprecated nur für Hinbernate
     */
    @Deprecated
    protected BikeData() {
    }

    /**
     * Typen der Fahrrader.
     */
    public enum Type {
        /** Männerrad */
        MENS_BIKE,
        /** Frauenrad */
        WOMENS_BIKE,
        /** Mountain-Bike */
        MOUNTAIN_BIKE,
        /** Tandem */
        TANDEM,
        /** Einrad */
        UNICYCLE,
        /** Holland-Rad */
        DUTCH,
        /** allgemeines Mädchenrad */
        GIRLS_GENERAL,
        /** allgemeines Jungenrad */
        BOYS_GENERAL,
        /** Tigerenten-Fahrrad */
        TIGER_DUCK,
        /** Pucky-Rad mit Fähnchen */
        PUCKY,
        /** Prinzessinen-Rad */
        GIRLY,
        /** Gangster-Rad */
        GANGSTER,
        /** Laufrad */
        BOGIE_WHEEL,
        /** Hässliches Rad */
        OFFENSIVE_COLOR
    }

    public Type getType() {
        return this.type;
    }

    public void setType(final Type type) {
        this.type = type;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getTireSize() {
        return tireSize;
    }

    public void setTireSize(int tireSize) {
        this.tireSize = tireSize;
    }

    public int getFrameHeight() {
        return frameHeight;
    }

    public void setFrameHeight(int bicycleFrameHeight) {
        this.frameHeight = bicycleFrameHeight;
    }

    public String getFrameNumber() {
        return frameNumber;
    }

    public void setFrameNumber(String frameNumber) {
        this.frameNumber = frameNumber;
    }

    /**
     * @return Builder zum Erstellen von Fahrrad-Daten.
     */
    public static final BikeDataBuilder getBikeDataBuilder() {
        return new BikeDataBuilder();
    }

    /**
     * Builder Pattern zur einfacheren initialisierung von Fahrrad-Daten
     */
    public static class BikeDataBuilder {
        private String color;
        private int tireSize;
        private int frameHeight;
        private String frameNumber;
        private Type type;

        public BikeDataBuilder withColor(String color) {
            this.color = color;
            return this;
        }

        public BikeDataBuilder withTireSize(int tireSize) {
            this.tireSize = tireSize;
            return this;
        }

        public BikeDataBuilder withFrameHeight(int frameHeight) {
            this.frameHeight = frameHeight;
            return this;
        }

        public BikeDataBuilder withFrameNumber(String frameNumber) {
            this.frameNumber = frameNumber;
            return this;
        }

        public BikeDataBuilder withType(Type type) {
            this.type = type;
            return this;
        }

        /**
         * @return das erzeugte BikeData Objekt mit den gesetzten Attributen.
         */
        public BikeData build() {
            return new BikeData(this.color, this.tireSize, this.frameHeight,
                    this.frameNumber, this.type);
        }
    }
}
