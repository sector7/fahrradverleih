package de.met.bikes.inventory.bike;

import static de.met.bikes.inventory.Item.ItemType.BIKE;

import javax.persistence.Embedded;
import javax.persistence.Entity;

import de.met.bikes.commons.Price;
import de.met.bikes.inventory.Item;
import de.met.bikes.inventory.ItemData;

/**
 * Klasse zur Modellierung eines Fahrrads.
 */
@Entity
public class Bike extends Item {

    private static final long serialVersionUID = 50576940467245312L;

    /**
     * Erstellt ein Fahrrad mit den zusätzlichen Informationen.
     * 
     * @param price Preis
     * @param itemData Leihartikel-Informationen
     * @param bikeData Fahrrad-Informationen
     */
    public Bike(Price price, ItemData itemData, BikeData bikeData) {
        super(price, itemData, BIKE);
        this.bikeData = bikeData;
    }

    /**
     * Konstruktor für Hibernate
     * 
     * @deprecated nur für Hinbernate
     */
    @Deprecated
    protected Bike() {

    }

    /** Informationen zum Fahrrad */
    @Embedded
    private BikeData bikeData;

    public final int getFrameHeight() {
        return this.bikeData.getFrameHeight();
    }

    public final String getColor() {
        return this.bikeData.getColor();
    }

    public final String getFrameNumber() {
        return this.bikeData.getFrameNumber();
    }

    public final int getTireSize() {
        return this.bikeData.getTireSize();
    }

    public final void setBicycleFrameHeight(final int bicycleFrameHeight) {
        this.bikeData.setFrameHeight(bicycleFrameHeight);
    }

    public final void setColor(final String color) {
        this.bikeData.setColor(color);
    }

    public final void setFrameNumber(final String frameNumber) {
        this.bikeData.setFrameNumber(frameNumber);
    }

    public final void setTireSize(final int tireSize) {
        this.bikeData.setTireSize(tireSize);
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(this.getClass().getSimpleName()).append(" ");
        buffer.append(this.bikeData.getType());
        buffer.append("\n").append(this.price);
        return buffer.toString();

    }
}
