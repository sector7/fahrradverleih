package de.met.bikes.inventory.equipment;

import static de.met.bikes.inventory.Item.ItemType.EQUIPMENT;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import de.met.bikes.commons.Price;
import de.met.bikes.inventory.Item;
import de.met.bikes.inventory.ItemData;

/**
 * Klasse zur Modellierung von Fahrradzubehör
 */
@Entity
public abstract class Equipment extends Item {

    private static final long serialVersionUID = -6590172628718074452L;

    /**
     * Erstellt Fahrradzubehör.
     *
     * @param price Preis
     * @param itemData weitere Informationen
     * @param equipmentType Typ des Equipments
     */
    public Equipment(final Price price, final ItemData itemData, EquipmentType equipmentType) {
        super(price, itemData, EQUIPMENT);
        this.equipmentType = equipmentType;
    }

    /** Zubehör-Typ */
    @Enumerated(EnumType.STRING)
    private EquipmentType equipmentType;

    /** Typ des Zubehörs */
    public enum EquipmentType {
        /** Korb */
        BASKET,
        /** Kindersitz */
        CHILD_SEAT,
        /** Helm */
        HELMET,
        /** Anhänger */
        TRAILER,
        /** Stützräder */
        TRAINING_WHEELS
    }

    /**
     * Leerer Konstruktor für Hibernate.
     *
     * @deprecated nur für Hinbernate
     */
    @Deprecated
    protected Equipment() {
    }

    public EquipmentType getEquipmentType() {
        return this.equipmentType;
    }

    public void setEquipmentType(final EquipmentType equipmentType) {
        this.equipmentType = equipmentType;
    }

}
