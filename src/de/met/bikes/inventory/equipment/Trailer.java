package de.met.bikes.inventory.equipment;

import static de.met.bikes.inventory.equipment.Equipment.EquipmentType.TRAILER;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import de.met.bikes.commons.Price;
import de.met.bikes.inventory.ItemData;

/**
 * Klasse zur Modellierung von Fahrradanhängern
 */
@Entity
public class Trailer extends Equipment {

    private static final long serialVersionUID = 7419715279396686136L;

    /** Typ des Anhängers */
    @Enumerated(EnumType.STRING)
    private Type type;

    /** Anhängertyp */
    public enum Type {
        /** Anhänger mit Platz für ein Kind */
        ONE_CHILD,
        /** Anhänger mit Platz für zwei Kinder */
        TWO_CHILDREN,
        /** Anhänger mit Platz für Zeug */
        STUFF
    }

    /**
     * Erstellt einen Fahrradanhänger mit den spezifischen Informationen.
     * 
     * @param price Preis
     * @param itemData weitere Informationen
     * @param type Typ
     */
    public Trailer(Price price, ItemData itemData, Type type) {
        super(price, itemData, TRAILER);
        this.type = type;
    }

    /**
     * Leerer Konstruktor für Hibernate.
     * 
     * @deprecated nur für Hinbernate
     */
    @Deprecated
    protected Trailer() {
    }

    public final Type getType() {
        return this.type;
    }

    public final void setType(final Type type) {
        this.type = type;
    }
}
