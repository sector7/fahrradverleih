package de.met.bikes.inventory.equipment;

import static de.met.bikes.inventory.equipment.Equipment.EquipmentType.BASKET;

import javax.persistence.Entity;

import de.met.bikes.commons.Price;
import de.met.bikes.inventory.ItemData;

/**
 * Klasse zur Modellierung eines Fahrradkorbs.
 */
@Entity
public class Basket extends Equipment {

    private static final long serialVersionUID = -8161301883420567529L;

    /**
     * Erstellt einen Fahrradkorb mit den spezifischen Informationen.
     *
     * @param price Preis
     * @param itemData weitere Informationen
     */
    public Basket(final Price price, final ItemData itemData) {
        super(price, itemData, BASKET);
    }

    /**
     * Leerer Konstruktor für Hibernate.
     *
     * @deprecated nur für Hibernate
     */
    @Deprecated
    public Basket() {

    }
}
