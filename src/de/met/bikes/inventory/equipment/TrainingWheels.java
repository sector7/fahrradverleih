package de.met.bikes.inventory.equipment;

import static de.met.bikes.inventory.equipment.Equipment.EquipmentType.TRAINING_WHEELS;

import javax.persistence.Entity;

import de.met.bikes.commons.Price;
import de.met.bikes.inventory.ItemData;

/**
 * Klasse zur Repräsentation von Stützrädern.
 */
@Entity
public class TrainingWheels extends Equipment {

    private static final long serialVersionUID = 2191922657268241157L;

    /**
     * Erstellt einen Fahrradkorb mit den spezifischen Informationen.
     * 
     * @param price Preis
     * @param itemData weitere Informationen
     */
    public TrainingWheels(Price price, ItemData itemData) {
        super(price, itemData, TRAINING_WHEELS);
    }

    /**
     * Leerer Konstruktor für Hibernate.
     * 
     * @deprecated nur für Hinbernate
     */
    @Deprecated
    public TrainingWheels() {

    }
}
