package de.met.bikes.inventory.equipment;

import static de.met.bikes.inventory.equipment.Equipment.EquipmentType.CHILD_SEAT;

import javax.persistence.Entity;

import de.met.bikes.commons.Price;
import de.met.bikes.inventory.ItemData;

/**
 * Klasse zur Modellierung eines Kindersitz.
 */
@Entity
public class ChildSeat extends Equipment {

    private static final long serialVersionUID = -6271783799197733244L;

    /**
     * Erstellt einen Kindersitz mit den spezifischen Informationen.
     * 
     * @param price Preis
     * @param itemData weitere Informationen
     */
    public ChildSeat(Price price, ItemData itemData) {
        super(price, itemData, CHILD_SEAT);
    }

    /**
     * Leerer Konstruktor für Hibernate.
     * 
     * @deprecated nur für Hinbernate
     */
    @Deprecated
    public ChildSeat() {

    }
}
