package de.met.bikes.inventory.equipment;

import static de.met.bikes.inventory.equipment.Equipment.EquipmentType.HELMET;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import de.met.bikes.commons.Price;
import de.met.bikes.inventory.ItemData;

/**
 * Klasse zur Modellierung eines Fahrradhelms.
 */
@Entity
public class Helmet extends Equipment {

    private static final long serialVersionUID = -7993486531246506073L;

    /**
     * Erstellt einen Helm mit den spezifischen Informationen.
     * 
     * @param price Preis
     * @param itemData weitere Informationen
     * @param color Farbe
     * @param size Größe
     */
    public Helmet(Price price, ItemData itemData, String color, Size size) {
        super(price, itemData, HELMET);
        this.color = color;
        this.size = size;
    }

    /**
     * Leerer Konstruktor für Hibernate.
     * 
     * @deprecated nur für Hinbernate
     */
    @Deprecated
    protected Helmet() {

    }

    /** Verfügbare Helmgrößen */
    public enum Size {
        XXXS, XXS, XS, S, M, L, XL, XXL
    }

    /** Farbe des Helms */
    private String color;

    /** Größe des Helms */
    @Enumerated(EnumType.STRING)
    private Size size;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

}
