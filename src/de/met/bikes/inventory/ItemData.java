package de.met.bikes.inventory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Transient;

import org.joda.time.Instant;

import de.met.bikes.action.Action;

/**
 * Spezifische Informationen eines Leihartikels.
 */
@Entity
public class ItemData implements Serializable {

    private static final long serialVersionUID = 1585853625872210534L;

    /** Referenz-ID */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    /** Modellbezeichnung */
    @Embedded
    private Model model;

    /** Historie des Artikels und zukünftige Ausleihen */
    @ManyToMany(mappedBy = "products")
    protected List<Action> actions;

    /** Kaufdatum */
    @Transient
    private Instant purchaseInstant;

    /** Workaround für Hibernate. */
    protected long purchaseDateTimestamp;

    /** Anschaffungspreis */
    private double purchasePrice;

    /**
     * Modellierung des Modells eines Leihartikels.
     */
    public static final class Model {
        /** Name des Herstellers */
        private String manufacturer;
        /** Modellnummer */
        private String modelNumber;

        /**
         * Konstruiert ein Modell.
         */
        public Model(final String manufacturer, final String modelNumber) {
            this.manufacturer = manufacturer;
            this.modelNumber = modelNumber;
        }

        /**
         * Leerer Konstruktor für Hibernate.
         *
         * @deprecated nur für Hibernate
         */
        @SuppressWarnings("unused")
        @Deprecated
        private Model() {
        }

        public final String getManufacturer() {
            return this.manufacturer;
        }

        public final String getModelNumber() {
            return this.modelNumber;
        }

        public final void setManufacturer(final String manufacturer) {
            this.manufacturer = manufacturer;
        }

        public final void setModelNumber(final String modelNumber) {
            this.modelNumber = modelNumber;
        }
    }

    /**
     * Builder zum erzeugen von Item-Informationen.
     */
    public static final class ItemDataBuilder {
        private Model model;
        private List<Action> states = new ArrayList<>();
        private Instant purchaseDate;
        private double purchasePrice;

        public ItemDataBuilder withModel(final Model model) {
            this.model = model;
            return this;
        }

        public ItemDataBuilder withStates(final List<Action> states) {
            this.states = states;
            return this;
        }

        public ItemDataBuilder withPurchaseDate(final Instant purchaseDate) {
            this.purchaseDate = purchaseDate;
            return this;
        }

        public ItemDataBuilder withPurchasePrice(final double purchasePrice) {
            this.purchasePrice = purchasePrice;
            return this;
        }

        /**
         * @return das Item-Datenobjekt mit den gesetzten Informationen
         */
        public ItemData build() {
            return new ItemData(this.model, this.states, this.purchaseDate,
                    this.purchasePrice);
        }
    }

    /**
     * Erstellt und initalisiert die Item spezifischen Informationen.
     *
     * @param model Modell
     * @param actions Zustände
     * @param purchaseDate Kaufdatum
     * @param purchasePrice Kaufpreis
     */
    public ItemData(final Model model, final List<Action> actions, final Instant purchaseDate,
            final double purchasePrice) {
        super();
        this.model = model;
        this.actions = actions == null ? new ArrayList<Action>() : actions;
        this.purchaseInstant = purchaseDate == null ? Instant.now() : purchaseDate;
        this.purchaseDateTimestamp = this.purchaseInstant.getMillis();
        this.purchasePrice = purchasePrice;
    }

    /**
     * Erstellt und initalisiert die Item spezifischen Informationen.
     *
     * @param model Modell
     * @param actions Zustände
     * @param purchasePrice Kaufpreis
     */
    public ItemData(final Model model, final List<Action> states, final double purchasePrice) {
        super();
        this.model = model;
        this.actions = states;
        this.purchaseInstant = Instant.now();
        this.purchaseDateTimestamp = this.purchaseInstant.getMillis();
        this.purchasePrice = purchasePrice;
    }

    /**
     * Leerer Konstruktor für Hibernate.
     *
     * @deprecated nur für Hibernate
     */
    @Deprecated
    public ItemData() {
    }

    public static final ItemDataBuilder getItemDataBuilder() {
        return new ItemDataBuilder();
    }

    public Model getModel() {
        return this.model;
    }

    public void setModel(final Model model) {
        this.model = model;
    }

    public List<Action> getActions() {
        return this.actions;
    }

    public void setActions(final List<Action> actions) {
        this.actions = actions;
    }

    @Transient
    public Instant getPurchaseInstant() {
        return this.purchaseInstant;
    }

    @Transient
    public void setPurchaseInstant(final Instant purchaseInstant) {
        this.purchaseInstant = purchaseInstant;
        this.purchaseDateTimestamp = purchaseInstant.getMillis();
    }

    public double getPurchasePrice() {
        return this.purchasePrice;
    }

    public void setPurchasePrice(final double purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public long getPurchaseDateTimestamp() {
        return this.purchaseDateTimestamp;
    }

    public void setPurchaseDateTimestamp(final long purchaseDateTimestamp) {
        this.purchaseDateTimestamp = purchaseDateTimestamp;
        this.purchaseInstant = new Instant(purchaseDateTimestamp);
    }

    public long getId() {
        return this.id;
    }

    public void setId(final long id) {
        this.id = id;
    }
}
