package de.met.bikes.inventory;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;

import org.joda.time.DateTime;
import org.joda.time.Instant;
import org.joda.time.Interval;

import de.met.bikes.action.Action;
import de.met.bikes.action.Discharge;
import de.met.bikes.action.Maintenance;
import de.met.bikes.action.Rental;
import de.met.bikes.action.Reservation;
import de.met.bikes.commons.Pair;
import de.met.bikes.commons.Price;
import de.met.bikes.contacts.Customer;
import de.met.bikes.exceptions.ItemDischargedException;
import de.met.bikes.exceptions.ItemInMaintenanceException;
import de.met.bikes.exceptions.ItemRentException;
import de.met.bikes.exceptions.ItemReservedException;
import de.met.bikes.exceptions.MaxUnaccomplishedReservationsReachedException;
import de.met.bikes.exceptions.MethodConstraintException;
import de.met.bikes.exceptions.RentalNotFoundException;
import de.met.bikes.exceptions.ReservationNotFoundException;
import de.met.bikes.exceptions.TimeBeginsInPastException;
import de.met.bikes.inventory.ItemData.Model;
import de.met.bikes.offer.Product;

@Entity
public abstract class Item extends Product {

    private static final long serialVersionUID = -7220296118042166140L;

    /** Informationen zum Item */
    @ManyToOne(cascade = CascadeType.ALL)
    protected ItemData itemData;

    /**
     * Erstellt das Item mit gegebenem Preis und den weiteren Informationen.
     *
     * @param price Preis
     * @param itemData spezifische Informationen
     * @param itemType Typ des Items
     */
    public Item(final Price price, final ItemData itemData, ItemType itemType) {
        super(price);
        this.itemData = itemData;
        this.itemType = itemType;
    }

    /**
     * Leerer Konstruktor für Hibernate.
     *
     * @deprecated nur für Hibernate
     */
    @Deprecated
    public Item() {
    }

    /** Typ des Artikels */
    @Enumerated(EnumType.STRING)
    private ItemType itemType;

    /** Obertyp der Artikel */
    public enum ItemType {
        /** Zubehör */
        EQUIPMENT,
        /** Fahrrad */
        BIKE
    }

    /**
     * Gibt zurück, ob der Leihartikel im angegebenen Zeitraum verfügbar ist.
     *
     * @param interval Zu überprüfender Zeitraum
     * @return (true, null) wenn verfügbar, (false, MethodConstraintException("Grund")) wenn nicht
     */
    public Pair<Boolean, MethodConstraintException> available(final Interval interval) {
        boolean res = true;
        MethodConstraintException e = null;

        for (final Action s : this.itemData.getActions())
            if (s.getInterval().overlaps(interval)) {
                switch (s.getType()) {
                case DISCHARGE:
                    e = new ItemDischargedException(((Discharge) s).getCause().toString()
                            + " (" + ((Discharge) s).getAdditionalInformation() + ")");
                    break;
                case MAINTENANCE:
                    e = new ItemInMaintenanceException(((Maintenance) s).getCause());
                    break;
                case RENTAL:
                    e = new ItemRentException();
                    break;
                case RESERVATION:
                    e = new ItemReservedException();
                    break;
                default:
                    e = new MethodConstraintException("Item is unavailable for unknown reason.");
                    break;
                }

                res = false;
            }

        return new Pair<Boolean, MethodConstraintException>(res, e);
    }

    public final Model getModel() {
        return this.itemData.getModel();
    }

    public final Instant getPurchaseDate() {
        return this.itemData.getPurchaseInstant();
    }

    public final double getPurchasePrice() {
        return this.itemData.getPurchasePrice();
    }

    public ItemData getItemData() {
        return this.itemData;
    }

    public void setItemData(final ItemData itemData) {
        this.itemData = itemData;
    }

    public ItemType getItemType() {
        return itemType;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    /**
     * Markiert den Leihartikel als ausgeliehen.
     *
     * @param rental
     * @throws MethodConstraintException
     */
    @Override
    public void rent(final Rental rental) throws MethodConstraintException {
        final Pair<Boolean, MethodConstraintException> available = available(rental.getInterval());
        if (!available.first)
            throw available.second;
        else if (rental.getInterval().isBeforeNow())
            throw new TimeBeginsInPastException(
                    "Ein Leihartikel kann nicht in der Vergangenheit ausgeliehen werden.");
        else {
            this.itemData.getActions().add(rental);
        }
    }

    /**
     * Markiert einen Leihartikel als vorzeitig zurückgegeben (i.e. ändert den End-Zeitpunkt).
     *
     * @param rental Ausleihe
     * @param returned Zeitpunkt der Rückgabe
     * @throws RentalNotFoundException
     */
    public void earlyReturn(final Rental rental, final DateTime returned)
            throws RentalNotFoundException {
        if (!this.itemData.getActions().contains(rental))
            throw new RentalNotFoundException();

        rental.setInterval(new Interval(rental.getInterval().getStart(), returned));
    }

    /**
     * Wandelt eine Reservierung in eine Ausleihe um.
     *
     * @param reservation Umzuwandelnde Reservierung
     * @param discount Gewährter Rabatt
     * @throws MethodConstraintException
     */
    public void pickup(final Reservation reservation, final double discount)
            throws MethodConstraintException {
        // Entfernt den Reservierungs-Status
        // Fügt einen Ausleih-Status mit denselben Daten ein
        removeReservation(reservation);
        reservation.makeRental(discount);
    }

    /**
     * Wandelt eine Reservierung in eine Ausleihe um.
     *
     * @param reservation Umzuwandelnde Reservierung
     * @throws MethodConstraintException
     */
    public void pickup(final Reservation reservation) throws MethodConstraintException {
        pickup(reservation, 0.0f);
    }

    /**
     * Markiert eine Reservierung als nicht abgeholt.
     *
     * @param reservation betroffene Reservierung
     * @throws ReservationNotFoundException
     */
    public void reservationNotAccomplished(final Reservation reservation)
            throws ReservationNotFoundException {
        reservation.getCustomer().incUnaccomplishedReservations();
        removeReservation(reservation);
    }

    /**
     * @return Kunde, der den Artikel zuletzt ausgeliehen hatte
     */
    public Customer lastlyRentBy() {
        for (int i = this.itemData.actions.size() - 1; i >= 0; i--) {
            final Action action = this.itemData.actions.get(i);
            if (Action.Type.RENTAL.equals(action.getType()))
                return ((Rental) action).getCustomer();
        }
        return null;
    }

    /**
     * Entfernt die Reservierung.
     *
     * @param reservation Reservierung
     * @throws ReservationNotFoundException
     */
    public void removeReservation(final Reservation reservation)
            throws ReservationNotFoundException {
        if (!this.itemData.getActions().contains(reservation))
            throw new ReservationNotFoundException();

        this.itemData.getActions().remove(reservation);
    }

    /**
     * Schickt einen Leihartikel in die Reparatur.
     *
     * @throws MethodConstraintException
     */
    public void repair(final Maintenance maintenance) throws MethodConstraintException {
        final Pair<Boolean, MethodConstraintException> available =
                available(maintenance.getInterval());

        if (!available.first)
            throw available.second;

        else if (maintenance.getInterval().isBeforeNow())
            throw new TimeBeginsInPastException(
                    "Ein Leihartikel kann nicht in der Vergangenheit repariert werden.");
        else {
            this.itemData.getActions().add(maintenance);
        }
    }

    /**
     * Markiert einen Leihartikel als ausgemustert.
     *
     * @throws MethodConstraintException
     */
    public void discharge(final Discharge discharge) throws MethodConstraintException {
        // Status von jetzt bis MAXINT setzen
        final Pair<Boolean, MethodConstraintException> available =
                available(discharge.getInterval());

        if (!available.first)
            throw available.second;

        else if (discharge.getInterval().isBeforeNow())
            throw new TimeBeginsInPastException(
                    "Ein Leihartikel kann nicht in der Vergangenheit ausgemustert werden.");
        else {
            this.itemData.getActions().add(discharge);
        }
    }

    /**
     * Reserviert einen Artikel.
     *
     * @throws MethodConstraintException
     */
    @Override
    public void book(final Reservation reservation) throws MethodConstraintException {
        if (reservation.getCustomer().hasReachedMaxUnaccomplishedReservations())
            throw new MaxUnaccomplishedReservationsReachedException(
                    "Der Kunde darf nur noch direkt ausleihen, weil er zu viele Reservierungen nicht wahrgenommen hat.");

        final Pair<Boolean, MethodConstraintException> available =
                available(reservation.getInterval());

        if (!available.first)
            throw available.second;

        else if (reservation.getInterval().isBeforeNow())
            throw new TimeBeginsInPastException(
                    "Ein Leihartikel kann nicht in der Vergangenheit reserviert werden.");
        else {
            this.itemData.getActions().add(reservation);
        }
    }

    public final void setModel(final Model model) {
        this.itemData.setModel(model);
    }

    public final void setPurchaseDate(final Instant purchaseDate) {
        this.itemData.setPurchaseInstant(purchaseDate);
    }

    public final void setPurchasePrice(final double purchasePrice) {
        this.itemData.setPurchasePrice(purchasePrice);
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "\n" + this.price.toString();
    }
}
