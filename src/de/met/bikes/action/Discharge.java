package de.met.bikes.action;

import static de.met.bikes.action.Action.Type.DISCHARGE;
import static java.util.Arrays.asList;

import java.util.Arrays;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import org.joda.time.DateTime;
import org.joda.time.Interval;

import de.met.bikes.offer.Product;

/**
 * Repräsenation der Ausmusterung von Fahrrädern.
 */
@Entity
public class Discharge extends Action {

    private static final long serialVersionUID = 2888572166441585422L;

    /** Zusätzliche Informationen */
    private String additionalInformation;

    /** Grund für die Ausmusterung. */
    @Enumerated(EnumType.STRING)
    private Cause cause;

    /**
     * Allgemeine Gründe für die Ausmusterung
     */
    public enum Cause {
        /** Fahrrad wurde gestohlen */
        STOLEN,
        /** Fahrrad war defekt */
        DAMAGED,
        /** Nicht verkehrssicher */
        NOT_ROADWORTHY,
        /** andere Gründe */
        OTHERS
    }

    /**
     * Setzt die Informationen für die Ausmusterung.
     *
     * @param interval Zeitraum
     * @param cause Grund
     * @param additionalInformation zusätzliche Infos
     * @param products Produkte
     */
    public Discharge(final Interval interval, final Cause cause,
            final String additionalInformation, List<Product> products) {
        super(interval);
        this.cause = cause;
        this.additionalInformation = additionalInformation;
        this.type = DISCHARGE;
        this.products = products;
    }

    /**
     * Setzt die Informationen für die Ausmusterung.
     *
     * @param interval Zeitraum
     * @param cause Grund
     * @param additionalInformation zusätzliche Infos
     * @param product Produkt
     */
    public Discharge(final Interval interval, final Cause cause,
            final String additionalInformation, Product product) {
        this(interval, cause, additionalInformation, asList(product));
    }

    /**
     * Setzt die Informationen für die Ausmusterung.
     *
     * @param cause Grund
     * @param additionalInformation zusätzliche Infos
     * @param product Product
     */
    public Discharge(final Cause cause, final String additionalInformation, List<Product> products) {
        this(new Interval(DateTime.now().getMillis(), Long.MAX_VALUE), cause,
                additionalInformation, products);
    }

    /**
     * Setzt die Informationen für die Ausmusterung.
     *
     * @param cause Grund
     * @param additionalInformation zusätzliche Infos
     * @param product Product
     */
    public Discharge(final Cause cause, final String additionalInformation, Product product) {
        this(new Interval(DateTime.now().getMillis(), Long.MAX_VALUE), cause,
                additionalInformation, Arrays.asList(product));
    }

    /**
     * Setzt die Informationen für die Ausmusterung.
     *
     * @param cause Grund
     * @param additionalInformation zusätzliche Infos
     * @product Produkt
     */
    public Discharge(final Cause cause, Product product) {
        this(cause, "", product);
    }

    /**
     * Setzt die Informationen für die Ausmusterung.
     *
     * @param cause Grund
     * @param additionalInformation zusätzliche Infos
     * @param products Produkte
     */
    public Discharge(final Cause cause, List<Product> products) {
        this(cause, "", products);
    }

    /**
     * Leerer Konstruktor für Hibernate.
     *
     * @deprecated nur für Hibernate
     */
    @Deprecated
    protected Discharge() {
    }

    public String getAdditionalInformation() {
        return this.additionalInformation;
    }

    public void setAdditionalInformation(final String additionalInformation) {
        this.additionalInformation = additionalInformation;
    }

    public Cause getCause() {
        return this.cause;
    }

    public void setCause(final Cause cause) {
        this.cause = cause;
    }

    @Override
    public Type getType() {
        return Type.DISCHARGE;
    }

}
