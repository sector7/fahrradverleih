package de.met.bikes.action;

import static de.met.bikes.action.Action.Type.RESERVATION;
import static java.util.Arrays.asList;

import java.util.List;

import javax.persistence.Entity;

import org.joda.time.Interval;

import de.met.bikes.contacts.Customer;
import de.met.bikes.exceptions.MethodConstraintException;
import de.met.bikes.offer.Product;

/**
 * Klasse zur Repräsentation des einer Reservierung.
 */
@Entity
public class Reservation extends CustomerRelatedAction {

    private static final long serialVersionUID = 1237029006252494037L;

    /** Maximale Anzahl an Reservierungen, die ein Kunde nicht abgeholt haben darf */
    public static final int MAX_UNACCOMPLISHED_RESERVATIONS = 10;

    /**
     * Setzt die benötigten Informationen zur Reservierung.
     *
     * @param interval Zeitraum
     * @param customer zugehöriger Kunde
     * @param products zugehörige Produkte
     * @throws MethodConstraintException
     */
    public Reservation(final Interval interval, final Customer customer,
            final List<Product> products)
            throws MethodConstraintException {
        super(interval, customer, products);
        this.type = RESERVATION;
        for (final Product product : this.products) {
            product.book(this);
        }
    }

    /**
     * Setzt die benötigten Informationen zur Reservierung.
     *
     * @param interval Zeitraum
     * @param customer zugehöriger Kunde
     * @param products zugehörige Produkte
     * @throws MethodConstraintException
     */
    public Reservation(final Interval interval, final Customer customer,
            final Product product)
            throws MethodConstraintException {
        this(interval, customer, asList(product));
    }

    /**
     * Leerer Konstruktor für Hibernate.
     *
     * @deprecated nur für Hibernate
     */
    @SuppressWarnings("unused")
    @Deprecated
    private Reservation() {
    }

    @Override
    public Type getType() {
        return Type.RESERVATION;
    }

    /**
     * Wandelt diese Reservierung in eine Ausleihe um.
     *
     * @return Entsprechende Ausleihe
     * @throws MethodConstraintException
     */
    public Rental makeRental() throws MethodConstraintException {
        return new Rental(this.interval, this.customer, this.products);
    }

    /**
     * Wandelt diese Reservierung in eine Ausleihe um.
     *
     * @param discount Rabatt
     * @return Entsprechende Ausleihe
     * @throws MethodConstraintException
     */
    public Rental makeRental(final double discount) throws MethodConstraintException {
        return new Rental(this.interval, this.customer, this.products, discount);
    }

}
