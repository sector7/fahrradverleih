package de.met.bikes.action;

import static de.met.bikes.action.Action.Type.MAINTENANCE;
import static java.util.Arrays.asList;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import org.joda.time.Interval;

import de.met.bikes.contacts.RepairShop;
import de.met.bikes.offer.Product;

/**
 * Objekt zur Modellierung der Reparatur von Fahrrädern.
 */
@Entity
public class Maintenance extends Action {

    private static final long serialVersionUID = -5277462399862092224L;

    /**
     * Angabe zur ausführenden Werkstatt.
     */
    @ManyToOne(cascade = CascadeType.ALL)
    private RepairShop garage;

    /**
     * Reparaturgrund
     */
    private String cause;

    /**
     * Setzt die benötigten Informationen für eine Reparatur.
     *
     * @param garage zuständige Werkstatt
     * @param cause Reparatur-Grund
     * @param interval Zeitraum
     * @param products Produkte
     */
    public Maintenance(final RepairShop garage, List<Product> products, final String cause,
            final Interval interval) {
        super(interval);
        this.garage = garage;
        this.cause = cause;
        this.type = MAINTENANCE;
        this.products = products;
    }

    /**
     * Setzt die benötigten Informationen für eine Reparatur.
     *
     * @param garage zuständige Werkstatt
     * @param cause Reparatur-Grund
     * @param interval Zeitraum
     * @param product Produkt
     */
    public Maintenance(final RepairShop garage, Product product, final String cause,
            final Interval interval) {
        this(garage, asList(product), cause, interval);
    }

    /**
     * Leerer Konstruktor für Hibernate.
     *
     * @deprecated nur für Hibernate
     */
    @Deprecated
    protected Maintenance() {
    }

    public RepairShop getGarage() {
        return this.garage;
    }

    public void setGarage(final RepairShop garage) {
        this.garage = garage;
    }

    public String getCause() {
        return this.cause;
    }

    public void setCause(final String cause) {
        this.cause = cause;
    }

    @Override
    public Type getType() {
        return Type.MAINTENANCE;
    }
}
