package de.met.bikes.action;

import java.util.Arrays;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import org.joda.time.Interval;

import de.met.bikes.contacts.Customer;
import de.met.bikes.offer.Product;

/**
 * Abstrakte Oberklasse zur Darstellung kundenbezogener Aktionen.
 */
@Entity
public abstract class CustomerRelatedAction extends Action {

    private static final long serialVersionUID = 2635376897726572271L;

    /** zugehöriger Kunde */
    @ManyToOne(cascade = CascadeType.ALL)
    protected Customer customer;

    /**
     * Modellierung Kundenbezogener Aktionen, setzt die benötigten Informationen.
     *
     * @param interval Zeitspanne
     * @param customer Kunde
     * @param products Produkte
     */
    protected CustomerRelatedAction(final Interval interval, final Customer customer,
            final List<Product> products) {
        super(interval);
        this.customer = customer;
        this.products = products;
    }

    /**
     * Modellierung Kundenbezogener Aktionen, setzt die benötigten Informationen.
     *
     * @param interval Zeitspanne
     * @param customer Kunde
     * @param product Produkt
     */
    protected CustomerRelatedAction(final Interval interval, final Customer customer,
            final Product product) {
        this(interval, customer, Arrays.asList(product));
    }

    /**
     * Leerer Konstruktor für Hibernate.
     *
     * @deprecated nur für Hibernate
     */
    @Deprecated
    protected CustomerRelatedAction() {
    }

    public Customer getCustomer() {
        return this.customer;
    }

    public void setCustomer(final Customer customer) {
        this.customer = customer;
    }
}
