package de.met.bikes.action;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToMany;
import javax.persistence.Transient;

import org.joda.time.Interval;

import de.met.bikes.offer.Product;

/**
 * Abstrakte Oberklasse zur Darstellung von Aktionen.
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Action implements Serializable {

    private static final long serialVersionUID = 794221146822229043L;

    /** Referenz-ID */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    /** Betroffene Produkte */
    // @OneToMany(cascade = { CascadeType.ALL })
    @ManyToMany(cascade = { CascadeType.ALL })
    protected List<Product> products;

    /** Typ der Aktion. */
    public enum Type {
        /** Reservierung */
        RESERVATION,
        /** Ausleihe */
        RENTAL,
        /** Ausmusterung */
        DISCHARGE,
        /** Reparatur */
        MAINTENANCE
    }

    /** Typ der Aktion */
    @Enumerated(EnumType.STRING)
    protected Type type;

    /**
     * Zeitraum, für den der Aktion gilt.
     */
    @Transient
    protected Interval interval = null;

    /**
     * Beginn des Zeitraums zum Speichern in der Datenbank.
     */
    protected long timespanBegin;

    /**
     * Ende des Zeitraums zum Speichern in der Datenbank.
     */
    protected long timespanEnd;

    /**
     * Setzt den Zeitraum des Aktion.
     *
     * @param timespan Zeitraum
     */
    protected Action(final Interval interval) {
        this.interval = interval;
        this.timespanBegin = interval.getStartMillis();
        this.timespanEnd = interval.getEndMillis();
    }

    protected Action(final long timespanBegin, final long timespanEnd) {
        this.timespanBegin = timespanBegin;
        this.timespanEnd = timespanEnd;
        this.interval = new Interval(timespanBegin, timespanEnd);
    }

    public long getTimespanBegin() {
        return this.timespanBegin;
    }

    public void setTimespanBegin(final long timespanBegin) {
        this.timespanBegin = timespanBegin;

        if (this.timespanEnd > 0) {
            this.interval = new Interval(timespanBegin, this.timespanEnd);
        }

    }

    public long getTimespanEnd() {
        return this.timespanEnd;
    }

    public void setTimespanEnd(final long timespanEnd) {
        this.timespanEnd = timespanEnd;

        if (this.timespanBegin > 0) {
            this.interval = new Interval(this.timespanBegin, timespanEnd);
        }
    }

    /**
     * Leerer Konstruktor für Hibernate.
     * 
     * @deprecated Nur für Hibernate
     */
    @Deprecated
    protected Action() {
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    @Transient
    public Interval getInterval() {
        return this.interval;
    }

    @Transient
    public void setInterval(final Interval interval) {
        this.interval = interval;
    }

    public long getId() {
        return this.id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public List<Product> getProducts() {
        return this.products;
    }

    public void setProducts(final List<Product> products) {
        this.products = products;
    }
}
