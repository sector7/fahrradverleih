package de.met.bikes.action;

import static de.met.bikes.action.Action.Type.RENTAL;

import java.util.Arrays;
import java.util.List;

import javax.persistence.Entity;

import org.joda.time.Interval;

import de.met.bikes.contacts.Customer;
import de.met.bikes.exceptions.MethodConstraintException;
import de.met.bikes.offer.Product;

/**
 * Klasse zur Repräsentation einer Ausleihe.
 */
@Entity
public class Rental extends CustomerRelatedAction {

    private static final long serialVersionUID = -8237530913636699908L;

    /** Ausleih-Preis */
    private double price = 0.0f;

    /** Rabatt */
    private double discount = 0.0f;

    /** Betrag der Preisreduktion */
    private double priceReduction = 0.0f;

    /** Endpreis unter Abzug des Rabatts */
    private double endPrice = 0.0f;

    /**
     * Setzt die Informationen zu einer Ausleihe.
     *
     * @param Interval Zeitspanne
     * @param customer Kunde
     * @param products Produkte
     * @param discount Rabatt
     * @throws MethodConstraintException
     */
    public Rental(final Interval interval, final Customer customer, final List<Product> products,
            final double discount)
            throws MethodConstraintException {
        super(interval, customer, products);
        this.discount = discount;
        this.type = RENTAL;
        calcPrices();
        for (final Product product : products) {
            product.rent(this);
        }
    }

    /**
     * Setzt die Informationen zu einer Ausleihe.
     *
     * @param Interval Zeitspanne
     * @param customer Kunde
     * @param product Produkt
     * @param discount Rabatt
     * @throws MethodConstraintException
     */
    public Rental(final Interval interval, final Customer customer, final Product product,
            final double discount)
            throws MethodConstraintException {
        this(interval, customer, Arrays.asList(product));
    }

    /**
     * Setzt die Informationen zu einer Ausleihe.
     *
     * @param Interval Zeitspanne
     * @param customer Kunde
     * @param products Produkte
     * @throws MethodConstraintException
     */
    public Rental(final Interval interval, final Customer customer, final List<Product> products)
            throws MethodConstraintException {
        this(interval, customer, products, 0.0f);
    }

    /**
     * Setzt die Informationen zu einer Ausleihe.
     *
     * @param Interval Zeitspanne
     * @param customer Kunde
     * @param product Produkt
     * @param discount Rabatt
     * @throws MethodConstraintException
     */
    public Rental(final Interval interval, final Customer customer, final Product product)
            throws MethodConstraintException {
        this(interval, customer, product, 0.0f);
    }

    /**
     * Leerer Konstruktor für Hibernate.
     *
     * @deprecated nur für Hibernate
     */
    @Deprecated
    protected Rental() {
    }

    /**
     * Berechnet die Preise der Ausleihe.
     */
    public void calcPrices() {
        this.price = calcPrice();
        this.endPrice = calcEndPrice();
        this.priceReduction = this.price - this.endPrice;
    }

    /**
     * @return Gesamtpreis der Ausleihe auf Grundlage des Zeitraums und den auf dem Produkt
     *         angegebenen Preisen
     */
    private float calcPrice() {
        float price = 0.0f;
        final long rentalDays = calcRentalDays();
        final long rentalHours = calcRentalHours(rentalDays);
        for (final Product product : this.products) {
            price += rentalDays * product.getPrice().getPerDayPrice();
            price += rentalHours * product.getPrice().getPerHourPrice();
        }
        return price;
    }

    /**
     * @return Anzahl Tage der Ausleihe
     */
    private long calcRentalDays() {
        return this.interval.toDuration().getStandardDays();
    }

    /**
     * @param rentalDays Anzahl Tage der Ausleihe
     * @return Anzahl Stunden der Ausleihe
     */
    private long calcRentalHours(final long rentalDays) {
        final long remainingHoursInMs =
                this.interval.toInterval().getEnd().minusDays((int) rentalDays).getMillis()
                        - this.interval.getStartMillis();
        final long rentalHours =
                new Interval(this.interval.getStartMillis(), this.interval.getStartMillis()
                        + remainingHoursInMs)
                        .toDuration().getStandardHours();
        return rentalHours;
    }

    /**
     * @return Endpreis mit Abzug des Rabatts.
     */
    private double calcEndPrice() {
        return this.price - Math.floor(this.price * this.discount);
    }

    /**
     * @return String-Repräsentation einer Rechnung.
     */
    public String createInvoice() {
        final long rentalDays = calcRentalDays();
        final long rentalHours = calcRentalHours(rentalDays);
        final StringBuffer buffer = new StringBuffer();
        buffer.append("---- INVOICE ----").append("\n\n");
        buffer.append(this.customer);
        buffer.append("\nDauer: ").append(rentalDays).append(" d, ").append(rentalHours)
                .append(" h");
        buffer.append("\n\nEntliehene Produkte: ");
        for (final Product product : this.products) {
            buffer.append("\n- ").append(product);
        }
        buffer.append("\n______________________________________________");
        buffer.append("\nPreis: \t\t\t\t").append(this.price).append("€");
        buffer.append("\nRabatt: \t\t\t").append(this.discount * 100.0).append("%");
        buffer.append("\nReduktion: \t\t\t").append(this.priceReduction).append("€");
        buffer.append("\nEnd-Preis: \t\t\t").append(this.endPrice).append("€");
        return buffer.toString();
    }

    public double getPrice() {
        return this.price;
    }

    public void setPrice(final double price) {
        this.price = price;
    }

    public double getDiscount() {
        return this.discount;
    }

    public void setDiscount(final float discount) {
        this.discount = discount;
    }

    @Override
    public void setProducts(final List<Product> products) {
        super.setProducts(products);
        calcPrices();
    }

    @Override
    public Type getType() {
        return Type.RENTAL;
    }

    public double getPriceReduction() {
        return this.priceReduction;
    }

    public void setPriceReduction(final double priceReduction) {
        this.priceReduction = priceReduction;
    }

    public double getEndPrice() {
        return this.endPrice;
    }

    public void setEndPrice(final double endPrice) {
        this.endPrice = endPrice;
    }

    public void setDiscount(final double discount) {
        this.discount = discount;
    }

}
