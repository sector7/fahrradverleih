package de.met.bikes;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.joda.time.Interval;

import de.met.bikes.action.Action;
import de.met.bikes.action.CustomerRelatedAction;
import de.met.bikes.action.Discharge;
import de.met.bikes.action.Maintenance;
import de.met.bikes.action.Rental;
import de.met.bikes.action.Reservation;
import de.met.bikes.contacts.Contact;
import de.met.bikes.contacts.Customer;
import de.met.bikes.contacts.Employee;
import de.met.bikes.contacts.RepairShop;
import de.met.bikes.inventory.Item;
import de.met.bikes.inventory.ItemData;
import de.met.bikes.inventory.bike.Bike;
import de.met.bikes.inventory.equipment.Basket;
import de.met.bikes.inventory.equipment.ChildSeat;
import de.met.bikes.inventory.equipment.Helmet;
import de.met.bikes.inventory.equipment.Trailer;
import de.met.bikes.inventory.equipment.TrainingWheels;
import de.met.bikes.offer.Offer;
import de.met.bikes.offer.Product;

public class Hibernate {

    private static final SessionFactory sessionFactory;

    static {
        sessionFactory = new Configuration()
                .addPackage("de.fhw.bikes.contacts")
                .addAnnotatedClass(Contact.class)
                .addAnnotatedClass(Customer.class)
                .addAnnotatedClass(Employee.class)
                .addAnnotatedClass(Rental.class)
                .addAnnotatedClass(CustomerRelatedAction.class)
                .addAnnotatedClass(Product.class)
                .addAnnotatedClass(Item.class)
                .addAnnotatedClass(Bike.class)
                .addAnnotatedClass(Trailer.class)
                .addAnnotatedClass(TrainingWheels.class)
                .addAnnotatedClass(Basket.class)
                .addAnnotatedClass(ChildSeat.class)
                .addAnnotatedClass(Helmet.class)
                .addAnnotatedClass(ItemData.class)
                .addAnnotatedClass(RepairShop.class)
                .addAnnotatedClass(Offer.class)
                .addAnnotatedClass(Interval.class)
                .addAnnotatedClass(Action.class)
                .addAnnotatedClass(Discharge.class)
                .addAnnotatedClass(CustomerRelatedAction.class)
                .addAnnotatedClass(Maintenance.class)
                .addAnnotatedClass(Reservation.class)
                .configure().buildSessionFactory();
    }

    public static Session getSession()
            throws HibernateException {
        return sessionFactory.openSession();
    }

}
