#!/bin/bash

set -x
HIBROOT=/Users/uho/workspace-SS2008/hibernate-3.2
CP=.
CP=$CP:src
CP=$CP:bin
CP=$CP:$HIBROOT/hibernate3.jar
CP=$CP:$HIBROOT/lib/commons-logging-1.0.4.jar
CP=$CP:$HIBROOT/lib/commons-collections-2.1.1.jar
CP=$CP:$HIBROOT/lib/dom4j-1.6.1.jar
CFG=src/hibernate.cfg.xml
java -cp $CP org.hibernate.tool.hbm2ddl.SchemaExport --text --format --config="$CFG" $*