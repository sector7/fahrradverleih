/**
 * Test application for example
 * 
 * @author Sebastian Hennebrueder
 *         created Jan 16, 2006
 *         copyright 2006 by http://www.laliluna.de
 */

package oodb.example;

import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import oodb.hibernate.InitSessionFactory;

public class TestExample {

    private static Logger log = Logger.getLogger(TestExample.class);

    private static void createHoney(final Honey honey) {
        Transaction tx = null;
        final Session session = InitSessionFactory.getInstance().getCurrentSession();
        try {
            tx = session.beginTransaction();
            session.save(honey);
            tx.commit();
        } catch (final HibernateException e) {
            e.printStackTrace();
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
        }
    }

    private static void deleteHoney(final Honey honey) {
        Transaction tx = null;
        final Session session = InitSessionFactory.getInstance().getCurrentSession();
        try {
            tx = session.beginTransaction();
            session.delete(honey);
            tx.commit();
        } catch (final HibernateException e) {
            e.printStackTrace();
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
        }
    }

    private static void listHoney() {
        Transaction tx = null;
        final Session session = InitSessionFactory.getInstance().getCurrentSession();
        try {
            tx = session.beginTransaction();
            final List honeys = session.createQuery("select h from Honey as h").list();
            for (final Iterator iter = honeys.iterator(); iter.hasNext();) {
                final Honey element = (Honey) iter.next();
                log.debug(element);
            }
            tx.commit();
        } catch (final HibernateException e) {
            e.printStackTrace();
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }

        }
    }

    /**
     * @param args
     */
    public static void main(final String[] args) {
        final Honey forestHoney = new Honey();
        forestHoney.setName("forest honey");
        forestHoney.setTaste("very sweet");

        final Honey countryHoney = new Honey();
        countryHoney.setName("country honey");
        countryHoney.setTaste("tasty");

        createHoney(forestHoney);
        createHoney(countryHoney);

        // our instances have a primary key now:
        log.debug(forestHoney);
        log.debug(countryHoney);
        listHoney();

        // deleteHoney(forestHoney);
        // deleteHoney(countryHoney);
        listHoney();
    }
}
