package de.met.bikes;

import static de.met.bikes.inventory.ItemData.getItemDataBuilder;
import static org.joda.time.Instant.now;

import org.hibernate.Session;
import org.junit.Test;

import de.met.bikes.commons.Address;
import de.met.bikes.commons.Price;
import de.met.bikes.contacts.Customer;
import de.met.bikes.contacts.Employee;
import de.met.bikes.contacts.RepairShop;
import de.met.bikes.exceptions.MethodConstraintException;
import de.met.bikes.inventory.ItemData.Model;
import de.met.bikes.inventory.equipment.Basket;
import de.met.bikes.inventory.equipment.ChildSeat;
import de.met.bikes.inventory.equipment.Helmet;
import de.met.bikes.inventory.equipment.Trailer;
import de.met.bikes.inventory.equipment.TrainingWheels;

/**
 * Legt einige Test-Fixtures in die Fahrradverleih Datenbank.
 */
public class TestFixtures {

    @Test
    public void createFixtures_contacts() throws MethodConstraintException {
        new TestTransaction() {

            @Override
            public void then(Session session) throws MethodConstraintException {
                // sollte nicht rausfliegen
            }

            @Override
            public void when(Session session) throws MethodConstraintException {
                // Customer
                session.save(
                        new Customer(
                                "Albert Schnösel",
                                new Address("Schnöselallee", "1a", 22222, "Schnöselhausen")
                        ));

                // Werkstatt
                session.save(
                        new RepairShop(
                                "Fix All The Things",
                                new Address("Waschstraße", "123", 12345, "Wellingen")
                        ));

                // Employee
                session.save(
                        new Employee(
                                "Oberchef",
                                new Address(
                                        "Elbchaussee", "1", 10000, "Hamburg"),
                                Employee.Status.ACTIVE
                        ));
                session.save(
                        new Employee(
                                "Alihat Gekündigt",
                                new Address(
                                        "Osdorfer Landstraße", "10", 22607, "Hamburg"),
                                Employee.Status.QUIT
                        ));
                session.save(
                        new Employee(
                                "Jakob IstinPansion",
                                new Address(
                                        "Süderdeich", "68", 21129, "Hamburg"),
                                Employee.Status.RETIRED
                        ));
                session.save(
                        new Employee(
                                "Michel Isttot",
                                new Address(
                                        "Friedhofstraße", "666", 66666, "Hölle"),
                                Employee.Status.DEAD
                        ));
            }

        }.execute();
    }

    @Test
    public void createFixtures_equipment() throws MethodConstraintException {
        new TestTransaction() {

            @Override
            public void then(Session session) throws MethodConstraintException {
                // sollte nicht rausfliegen
            }

            @Override
            public void when(Session session) throws MethodConstraintException {
                session.save(
                        new Basket(
                                new Price(1.50, 3.50),
                                getItemDataBuilder()
                                        .withModel(new Model("Fahrradladen", "112233"))
                                        .withPurchaseDate(now())
                                        .withPurchasePrice(7.50)
                                        .build()
                        ));
                session.save(
                        new ChildSeat(
                                new Price(5.00, 10.00),
                                getItemDataBuilder()
                                        .withModel(new Model("Fahrradladen", "112233"))
                                        .withPurchaseDate(now())
                                        .withPurchasePrice(95.00)
                                        .build()
                        ));
                session.save(
                        new Helmet(
                                new Price(5.00, 10.00),
                                getItemDataBuilder()
                                        .withModel(new Model("Fahrradladen", "112233"))
                                        .withPurchaseDate(now())
                                        .withPurchasePrice(95.00)
                                        .build(),
                                "rot",
                                Helmet.Size.M
                        ));
                session.save(
                        new Trailer(
                                new Price(15.00, 30.00),
                                getItemDataBuilder()
                                        .withModel(new Model("Fahrradladen", "112233"))
                                        .withPurchaseDate(now())
                                        .withPurchasePrice(150.00)
                                        .build(),
                                Trailer.Type.TWO_CHILDREN
                        ));
                session.save(
                        new TrainingWheels(
                                new Price(2.50, 5.00),
                                getItemDataBuilder()
                                        .withModel(new Model("Fahrradladen", "112233"))
                                        .withPurchaseDate(now())
                                        .withPurchasePrice(95.00)
                                        .build()
                        ));
            }

        }.execute();
    }

}
