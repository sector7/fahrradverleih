package de.met.bikes.useCases;

import static de.met.bikes.useCases.TestUtils.CUSTOMER_MALTE;
import static de.met.bikes.useCases.TestUtils.getSampleBikeHerkules;
import static de.met.bikes.useCases.TestUtils.getSampleBikePegagus;
import static de.met.bikes.useCases.TestUtils.getSampleTrailer;
import static de.met.bikes.useCases.TestUtils.getSampleTrainingWheels;
import static org.joda.time.DateTime.now;
import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.hibernate.Session;
import org.joda.time.Interval;
import org.junit.Test;

import de.met.bikes.TestTransaction;
import de.met.bikes.action.Rental;
import de.met.bikes.commons.Price;
import de.met.bikes.exceptions.MethodConstraintException;
import de.met.bikes.offer.Offer;
import de.met.bikes.offer.Product;

/**
 * Testet das Erstellen von Rechnungen
 */
public class InvoiceTest {

    @Test
    public void testCalculatePrices() throws MethodConstraintException {
        new TestTransaction() {
            List<Product> products = Arrays.asList(
                    getSampleBikePegagus(),
                    getSampleTrainingWheels(),
                    new Offer(new Price(8.0, 35.0), Arrays.asList(getSampleBikeHerkules(),
                            getSampleTrailer())));
            Rental rental;

            @Override
            public void when(final Session session) throws MethodConstraintException {
                this.rental = new Rental(
                        new Interval(now(), now().plusDays(1)),
                        CUSTOMER_MALTE,
                        this.products, 0.1);
                session.save(this.rental);
            }

            @Override
            public void then(final Session session) throws MethodConstraintException {
                assertEquals(80.0, rental.getPrice(), 0.009);
                assertEquals(72.0, rental.getEndPrice(), 0.009);
                assertEquals(0.1, rental.getDiscount(), 0.009);
                assertEquals(8.0, rental.getPriceReduction(), 0.009);

                System.out.println(this.rental.createInvoice());
            }

        }.execute();
    }
}
