package de.met.bikes.useCases;

import static de.met.bikes.useCases.TestUtils.getSampleBikeHerkules;

import org.hibernate.Session;
import org.junit.Assert;
import org.junit.Test;

import de.met.bikes.TestTransaction;
import de.met.bikes.action.Discharge;
import de.met.bikes.exceptions.MethodConstraintException;
import de.met.bikes.inventory.bike.Bike;

/**
 * Testet die Ausmusterung von Fahrrädern.
 */
public class DischargeTest {

    @Test
    public void testDischarge() throws MethodConstraintException {

        new TestTransaction() {

            Bike bike = getSampleBikeHerkules();
            Discharge discharge = new Discharge(Discharge.Cause.DAMAGED, bike);

            @Override
            public void when(final Session session) throws MethodConstraintException {
                bike.discharge(discharge);
                session.save(bike);
            }

            @Override
            public void then(final Session session) throws MethodConstraintException {
                Assert.assertTrue(bike.getItemData().getActions().contains(discharge));
            }
        }.execute();
    }
}
