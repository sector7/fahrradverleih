package de.met.bikes.useCases;

import static de.met.bikes.useCases.TestUtils.CUSTOMER_ELLEN;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Session;
import org.junit.Test;

import de.met.bikes.TestTransaction;
import de.met.bikes.commons.Address;
import de.met.bikes.contacts.Contact;
import de.met.bikes.contacts.Customer;
import de.met.bikes.contacts.Employee;
import de.met.bikes.contacts.Employee.Status;
import de.met.bikes.contacts.RepairShop;
import de.met.bikes.exceptions.MethodConstraintException;

/**
 * Testet die Funktionalität rund um die Kontakte des Fahrradverleihs
 */
public class ContactTest {

    private final Address testAddress = new Address("Esinger Weg", "43", 25436, "Tornesch");
    private final Contact contact = new Customer("Malte", this.testAddress);

    @Test
    public final void testDeleteContact() throws MethodConstraintException {
        new TestTransaction() {

            @Override
            public void given(final Session s) {
                s.save(CUSTOMER_ELLEN);
            };

            @Override
            public void when(final Session s) {
                s.delete(CUSTOMER_ELLEN);
            }

            @Override
            public void then(final Session s) {
                assertEquals(0, s.createQuery("from Contact where Name = 'Ellen'").list().size());
            }
        }.execute();
    }

    @Test
    public void testAddCustomer() throws MethodConstraintException {
        new TestTransaction() {
            @Override
            public void when(final Session s) {
                s.save(ContactTest.this.contact);
            }

            @Override
            public void then(final Session s) {
                @SuppressWarnings("unchecked")
                final List<Contact> list =
                        s.createQuery("from Contact where Name = 'Malte'").list();
                assertEquals(1, list.size());
                assertEquals(ContactTest.this.contact, list.get(0));
            }
        }.execute();
    }

    @Test
    public void testAddEmployee() throws MethodConstraintException {
        new TestTransaction() {
            private Employee mike;

            @Override
            public void when(final Session s) {
                this.mike = new Employee("Mike the mechanic",
                        new Address("Ingenieursweg", "17-19", 12345, "Entenhausen"), Status.RETIRED);
                s.save(this.mike);
            }

            @Override
            public void then(final Session s) {
                @SuppressWarnings("unchecked")
                final List<Contact> list = s.createQuery("from Employee where Status = :retired")
                        .setParameter("retired", Status.RETIRED.name()).list();
                assertEquals(1, list.size());
                assertEquals(this.mike, list.get(0));
            }

        }.execute();
    }

    @Test
    public void testAddRepairShop() throws MethodConstraintException {
        new TestTransaction() {
            private RepairShop mike;

            @Override
            public void when(final Session s) {
                this.mike = new RepairShop("Mike the mechanic",
                        new Address("Ingenieursweg", "21a", 12345, "Entenhausen"));
                s.save(this.mike);
            }

            @Override
            public void then(final Session s) {
                @SuppressWarnings("unchecked")
                final List<Contact> list = s.createQuery("from RepairShop").list();
                assertTrue(list.contains(this.mike));
            }

        }.execute();
    }

    @Test
    public void testChangeCustomerAddress() throws MethodConstraintException {
        new TestTransaction() {
            Address oldAddress = new Address("Alte Straße", "89", 55555, "Altstadt");
            Address newAddress = new Address("Neue Straße", "15", 44444, "Neustadt");
            Customer c = new Customer("Ulrich Hoffmann", this.oldAddress);
            Serializable customerID;

            @Override
            public void given(final Session session) {
                this.customerID = session.save(this.c);
            }

            @Override
            public void when(final Session session) throws MethodConstraintException {
                final Customer hoffmann = session.get(Customer.class, this.customerID);
                hoffmann.setAddress(this.newAddress);
                session.save(hoffmann);
            }

            @Override
            public void then(final Session session) throws MethodConstraintException {
                final Customer hoffmann = session.get(Customer.class, this.customerID);
                assertEquals(this.newAddress, hoffmann.getAddress());
            }
        }.execute();
    }
}
