package de.met.bikes.useCases;

import static de.met.bikes.inventory.ItemData.getItemDataBuilder;
import static de.met.bikes.inventory.bike.BikeData.getBikeDataBuilder;

import org.joda.time.DateTime;
import org.joda.time.Interval;

import de.met.bikes.commons.Address;
import de.met.bikes.commons.Price;
import de.met.bikes.contacts.Customer;
import de.met.bikes.contacts.Employee;
import de.met.bikes.contacts.Employee.Status;
import de.met.bikes.contacts.RepairShop;
import de.met.bikes.inventory.ItemData.Model;
import de.met.bikes.inventory.bike.Bike;
import de.met.bikes.inventory.bike.BikeData.Type;
import de.met.bikes.inventory.equipment.Trailer;
import de.met.bikes.inventory.equipment.TrainingWheels;

public class TestUtils {

    public static final Customer CUSTOMER_ELLEN = new Customer(
            "Ellen", new Address("Osdorfer Weg", "21", 22607, "Hamburg"), 20);

    public static final Customer CUSTOMER_MALTE = new Customer(
            "Malte", new Address("Esinger Weg", "43", 25436, "Tornesch"));

    public static final Customer CUSTOMER_TIMO = new Customer(
            "Timo", new Address("Kleekamp", "54", 22339, "Hamburg"));

    public static final Employee EMPLOYEE_EIKE = new Employee(
            "Eike", new Address("Feldstraße", "143", 22880, "Wedel"), Status.ACTIVE);

    public static final RepairShop REPAIRSHOP_BOC = new RepairShop(
            "Bock auf Bike", new Address("Hauptstraße", "42-44", 20147, "Hamburg"));

    public static final Bike getSampleBikePegagus() {
        return new Bike(
                new Price(10.0, 35.0),
                getItemDataBuilder()
                        .withModel(new Model("Pegasus", "10"))
                        .withPurchasePrice(100.0)
                        .build(),
                getBikeDataBuilder()
                        .withColor("rot")
                        .withFrameHeight(42)
                        .withFrameNumber("13374711")
                        .withType(Type.OFFENSIVE_COLOR)
                        .build());
    }

    public static final Bike getSampleBikeHerkules() {
        return new Bike(
                new Price(5.0f, 35.0f),
                getItemDataBuilder()
                        .withModel(new Model("Herkules", "2"))
                        .withPurchasePrice(500.0)
                        .build(),
                getBikeDataBuilder()
                        .withColor("blau")
                        .withFrameHeight(39)
                        .withTireSize(28)
                        .withFrameNumber("p3n1zz")
                        .withType(Type.BOGIE_WHEEL)
                        .build());
    }

    public static final Price PRICE = new Price(5.0, 10.0);

    public static final TrainingWheels getSampleTrainingWheels() {
        return new TrainingWheels(PRICE, getItemDataBuilder()
                .withModel(new Model("Biquipment", "123"))
                .withPurchasePrice(15.50)
                .build());
    }

    public static final Trailer getSampleTrailer() {
        return new Trailer(PRICE, getItemDataBuilder()
                .withModel(new Model("Biquipment", "1234"))
                .withPurchasePrice(15.50)
                .build(),
                Trailer.Type.STUFF);
    }

    public static final Interval forOneDay() {
        return new Interval(DateTime.now(), DateTime.now().plusDays(1));
    }

}
