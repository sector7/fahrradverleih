package de.met.bikes.useCases;

import static de.met.bikes.useCases.TestUtils.REPAIRSHOP_BOC;
import static de.met.bikes.useCases.TestUtils.getSampleBikeHerkules;

import org.hibernate.Session;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.junit.Assert;
import org.junit.Test;

import de.met.bikes.TestTransaction;
import de.met.bikes.action.Maintenance;
import de.met.bikes.exceptions.MethodConstraintException;
import de.met.bikes.inventory.bike.Bike;

public class MaintenanceTest {

    @Test
    public void testMaintainance() throws MethodConstraintException {
        new TestTransaction() {

            private Maintenance maintenance;
            Bike bike = getSampleBikeHerkules();

            @Override
            public void when(final Session session) throws MethodConstraintException {
                maintenance = new Maintenance(
                        REPAIRSHOP_BOC,
                        bike,
                        "Vorderrad verbogen",
                        new Interval(new DateTime(), new DateTime().plusDays(5)));
                this.bike.repair(maintenance);
                session.save(this.bike);
            }

            @Override
            public void then(final Session session) throws MethodConstraintException {
                Assert.assertTrue(this.bike.getItemData().getActions().contains(maintenance));
            }
        }.execute();
    }

}
