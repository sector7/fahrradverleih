package de.met.bikes.useCases;

import static de.met.bikes.action.Reservation.MAX_UNACCOMPLISHED_RESERVATIONS;
import static de.met.bikes.useCases.TestUtils.CUSTOMER_MALTE;
import static de.met.bikes.useCases.TestUtils.CUSTOMER_TIMO;
import static de.met.bikes.useCases.TestUtils.forOneDay;
import static de.met.bikes.useCases.TestUtils.getSampleBikeHerkules;
import static de.met.bikes.useCases.TestUtils.getSampleTrainingWheels;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.hibernate.Session;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.junit.Test;

import de.met.bikes.TestTransaction;
import de.met.bikes.action.Action;
import de.met.bikes.action.Rental;
import de.met.bikes.action.Reservation;
import de.met.bikes.commons.Address;
import de.met.bikes.contacts.Customer;
import de.met.bikes.exceptions.ItemRentException;
import de.met.bikes.exceptions.MaxUnaccomplishedReservationsReachedException;
import de.met.bikes.exceptions.MethodConstraintException;
import de.met.bikes.inventory.bike.Bike;
import de.met.bikes.inventory.equipment.TrainingWheels;

public class ReservationTest {

    @Test
    public void testReservation() throws MethodConstraintException {
        new TestTransaction() {

            Bike bike = getSampleBikeHerkules();

            @Override
            public void then(Session session) throws MethodConstraintException {
                List<Action> actions = bike.getItemData().getActions();
                assertEquals(1, actions.size());
                assertEquals(Action.Type.RESERVATION, actions.get(0).getType());
            }

            @Override
            public void when(Session session) throws MethodConstraintException {
                Reservation reservation = new Reservation(
                        new Interval(DateTime.now().plusDays(7), DateTime.now().plusDays(8)),
                        CUSTOMER_MALTE,
                        bike
                        );
                session.save(reservation);
            }

        }.execute();
    }

    /**
     * Reserviert einen Artikel und leiht es anschließend aus.
     * 
     * @throws MethodConstraintException
     */
    @Test
    public void testPickupReservation() throws MethodConstraintException {
        new TestTransaction() {

            Bike bike = getSampleBikeHerkules();
            Reservation reservation = new Reservation(
                    new Interval(DateTime.now().plusDays(7), DateTime.now().plusDays(8)),
                    CUSTOMER_MALTE,
                    bike
                    );

            @Override
            public void then(Session session) throws MethodConstraintException {
                List<Action> actions = bike.getItemData().getActions();
                assertEquals(1, actions.size());
                assertEquals(Action.Type.RENTAL, actions.get(0).getType());
                session.save(bike);
            }

            @Override
            public void when(Session session) throws MethodConstraintException {
                // Reservierung soll in eine Ausleihe umgewandelt werden
                bike.pickup(reservation);
            }

        }.execute();

    }

    @Test
    public void testRemoveReservation() throws MethodConstraintException {
        new TestTransaction() {

            Bike bike = getSampleBikeHerkules();
            Reservation reservation = new Reservation(
                    forOneDay(),
                    CUSTOMER_MALTE,
                    bike
                    );

            @Override
            public void then(Session session) throws MethodConstraintException {
                assertEquals(0, bike.getItemData().getActions().size());
            }

            @Override
            public void when(Session session) throws MethodConstraintException {
                bike.removeReservation(reservation);
                session.save(reservation);
            }

        }.execute();
    }

    /**
     * Testet ob der Zähler unabgeholter Reservierungen beim Customer korrekt hochgzählt wird.
     * 
     * @throws MethodConstraintException
     */
    @Test
    public void testUnaccomplishedReservation() throws MethodConstraintException {
        new TestTransaction() {

            Customer badCustomer = new Customer("Horst", new Address("Industriestraße", "143",
                    22880, "Wedel"));
            Bike bike = getSampleBikeHerkules();
            Reservation reservation = new Reservation(
                    forOneDay(),
                    badCustomer,
                    bike
                    );

            @Override
            public void then(Session session) throws MethodConstraintException {
                assertEquals(1, badCustomer.getUnaccomplishedReservations());
                assertFalse(badCustomer.hasReachedMaxUnaccomplishedReservations());
            }

            @Override
            public void when(Session session) throws MethodConstraintException {
                bike.reservationNotAccomplished(reservation);
                session.save(reservation);
            }

        }.execute();
    }

    // Testen, dass ein User nicht mehr reservieren darf
    @Test(expected = MaxUnaccomplishedReservationsReachedException.class)
    public void testReservate_withMaximumUnaccomplishedReservateReached()
            throws MethodConstraintException {
        new TestTransaction() {
            Customer badCustomer = new Customer("Horst", new Address("Industriestraße", "143",
                    22880, "Wedel"));
            Bike bike = getSampleBikeHerkules();

            /**
             * Hilfsmethode zum reservieren des Fahrrads.
             * 
             * @throws MethodConstraintException
             */
            private void reservateBike() throws MethodConstraintException {
                Reservation reservation = new Reservation(
                        forOneDay(),
                        badCustomer,
                        bike
                        );
                bike.reservationNotAccomplished(reservation);
            }

            @Override
            public void given(Session session) throws MethodConstraintException {
                for (int i = 0; i <= MAX_UNACCOMPLISHED_RESERVATIONS; i++) {
                    reservateBike();
                }
                session.save(bike);
            }

            @Override
            public void then(Session session) throws MethodConstraintException {
                fail("Erwartete Exception ist nicht geflogen!");
            }

            @Override
            public void when(Session session) throws MethodConstraintException {
                assertTrue(badCustomer.hasReachedMaxUnaccomplishedReservations());
                // solle der Customer nicht mehr dürfen
                reservateBike();
            }

        }.execute();

    }

    /**
     * Prüft das Verhalten, dass ein ausgeliehener Artikel nicht reserviert werden kann.
     * 
     * @throws MethodConstraintException
     */
    @Test(expected = ItemRentException.class)
    public void testReservate_withRentItem() throws MethodConstraintException {
        new TestTransaction() {

            TrainingWheels trainingWheels = getSampleTrainingWheels();
            Rental rental = new Rental(
                    forOneDay(),
                    CUSTOMER_MALTE,
                    trainingWheels);

            @Override
            public void then(Session session) throws MethodConstraintException {
                fail("Erwartete Exception wurde nicht geworfen!");
            }

            @Override
            public void when(Session session) throws MethodConstraintException {
                new Reservation(
                        forOneDay(),
                        CUSTOMER_TIMO,
                        trainingWheels);
            }

        }.execute();
    }
}
