package de.met.bikes.useCases;

import static de.met.bikes.useCases.TestUtils.CUSTOMER_ELLEN;
import static de.met.bikes.useCases.TestUtils.CUSTOMER_MALTE;
import static de.met.bikes.useCases.TestUtils.CUSTOMER_TIMO;
import static de.met.bikes.useCases.TestUtils.REPAIRSHOP_BOC;
import static de.met.bikes.useCases.TestUtils.forOneDay;
import static de.met.bikes.useCases.TestUtils.getSampleBikeHerkules;
import static de.met.bikes.useCases.TestUtils.getSampleBikePegagus;
import static de.met.bikes.useCases.TestUtils.getSampleTrailer;
import static de.met.bikes.useCases.TestUtils.getSampleTrainingWheels;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.hibernate.Session;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.junit.Test;

import de.met.bikes.TestTransaction;
import de.met.bikes.action.Action;
import de.met.bikes.action.Discharge;
import de.met.bikes.action.Maintenance;
import de.met.bikes.action.Rental;
import de.met.bikes.action.Reservation;
import de.met.bikes.contacts.Customer;
import de.met.bikes.exceptions.ItemDischargedException;
import de.met.bikes.exceptions.ItemInMaintenanceException;
import de.met.bikes.exceptions.ItemReservedException;
import de.met.bikes.exceptions.MethodConstraintException;
import de.met.bikes.inventory.bike.Bike;
import de.met.bikes.inventory.equipment.Trailer;
import de.met.bikes.inventory.equipment.TrainingWheels;

/**
 * Tests rund um den Ausleih-Prozess. Mountainbike und Anhänger reservieren Selbiges abholen ->
 * Stati beider Artikel gesetzt Vorzeitig zurückbringen -> endDates wurden neu gesetzt
 */
public class RentalTest {

    @Test
    public void testRental() throws MethodConstraintException {

        new TestTransaction() {

            Rental rental;

            @Override
            public void when(final Session session) throws MethodConstraintException {
                rental = new Rental(
                        new Interval(DateTime.now(), DateTime.now().plusDays(2)),
                        CUSTOMER_TIMO,
                        getSampleBikeHerkules(),
                        0.0d);
                session.save(rental);
            }

            @Override
            public void then(final Session session) {
                assertEquals(Action.Type.RENTAL, ((Bike) rental.getProducts().get(0)).getItemData()
                        .getActions().get(0).getType());
            }
        }.execute();
    }

    /**
     * Testet, ob eine Reservierung korrekt in eine Ausleihe umgewandelt wird.
     * 
     * @throws MethodConstraintException
     */
    @Test
    public void testReservationAndRental() throws MethodConstraintException {
        new TestTransaction() {

            Bike bike = getSampleBikeHerkules();
            Reservation reservation = new Reservation(
                    new Interval(DateTime.now().plusDays(7), DateTime.now().plusDays(8)),
                    CUSTOMER_MALTE,
                    bike
                    );

            @Override
            public void then(Session session) throws MethodConstraintException {
                List<Action> actions = bike.getItemData().getActions();
                assertEquals(1, actions.size());
                assertEquals(Action.Type.RENTAL, actions.get(0).getType());
                session.save(bike);
            }

            @Override
            public void when(Session session) throws MethodConstraintException {
                // Reservierung soll in eine Ausleihe umgewandelt werden
                bike.pickup(reservation);
            }

        }.execute();
    }

    @Test
    public void testRental_earlyReturn() throws MethodConstraintException {
        new TestTransaction() {

            Bike bike = getSampleBikeHerkules();
            Rental rental = new Rental(
                    new Interval(DateTime.now(), DateTime.now().plusDays(7)),
                    CUSTOMER_MALTE,
                    bike
                    );

            @Override
            public void then(Session session) throws MethodConstraintException {
                // Soll dann wieder verfügbar sein
                assertTrue(bike.available(
                        new Interval(DateTime.now().plusDays(7), DateTime.now().plusDays(8))
                        ).first);
                session.save(bike);
            }

            @Override
            public void when(Session session) throws MethodConstraintException {
                // Einen Tag früher zurückgeben
                bike.earlyReturn(rental, rental.getInterval().getEnd().minusDays(1));
            }

        }.execute();
    }

    /**
     * Testet den Fehlerfall, dass ein reserviertes Element nicht ausgeliehen werden kann.
     * 
     * @throws MethodConstraintException
     */
    @Test(expected = ItemReservedException.class)
    public void testRental_reservatedItem() throws MethodConstraintException {
        new TestTransaction() {

            Trailer trailer = getSampleTrailer();

            @Override
            public void then(Session session) throws MethodConstraintException {
                fail("Erwartete Exception ist nicht geflogen!");
            }

            @Override
            public void when(Session session) throws MethodConstraintException {
                // Anhänger für eine Woche reservieren
                Reservation reservation = new Reservation(
                        new Interval(DateTime.now(), DateTime.now().plusDays(7)),
                        CUSTOMER_MALTE,
                        trailer
                        );
                session.save(reservation);
                // Versuch den Anhänger in der reservierten Zeit auszuleihen muss eine Exception
                // werfen
                new Rental(
                        new Interval(DateTime.now().plusDays(3), DateTime.now().plusDays(4)),
                        CUSTOMER_MALTE,
                        trailer);
            }
        }.execute();
    }

    /**
     * Testet den Fall, dass ein in Reparatur befindlicher Artikel nicht ausgeliehen werden kann.
     * 
     * @throws MethodConstraintException
     */
    @Test(expected = ItemInMaintenanceException.class)
    public void testRental_itemInMaintenance() throws MethodConstraintException {
        new TestTransaction() {

            Bike bike;

            @Override
            public void given(Session session) throws MethodConstraintException {
                bike = getSampleBikePegagus();
                Maintenance maintenance = new Maintenance(
                        REPAIRSHOP_BOC,
                        bike,
                        "Verbogener Rahmen",
                        new Interval(DateTime.now(), DateTime.now().plusDays(7))
                        );
                bike.repair(maintenance);
                session.save(bike);
            }

            @Override
            public void then(Session session) throws MethodConstraintException {
                fail("Erwartete Exception wurde nicht geworfen!");
            }

            @Override
            public void when(Session session) throws MethodConstraintException {
                new Rental(
                        forOneDay(),
                        CUSTOMER_MALTE,
                        bike);
            }

        }.execute();
    }

    /**
     * Testet den Fehlerfall, dass ein ausgemustertes Element ausgeliehen werden soll.
     * 
     * @throws MethodConstraintException
     */
    @Test(expected = ItemDischargedException.class)
    public void testRental_dischargedItem() throws MethodConstraintException {
        new TestTransaction() {

            Bike bike = getSampleBikeHerkules();

            @Override
            public void then(Session session) throws MethodConstraintException {
                fail("Erwartete Exception ist nicht geflogen!");
            }

            @Override
            public void when(Session session) throws MethodConstraintException {
                // Ausmustern
                Discharge discharge =
                        new Discharge(Discharge.Cause.NOT_ROADWORTHY, "Bremsen kaputt", bike);
                bike.discharge(discharge);
                session.save(bike);
                // Ausleihen
                new Rental(
                        forOneDay(),
                        CUSTOMER_TIMO,
                        bike);
            }
        }.execute();
    }

    @Test
    public void testLastRentBy_wasntOrdered() throws MethodConstraintException {
        assertEquals(null, getSampleTrainingWheels().lastlyRentBy());
    }

    @Test
    public void testLastRentBy_wasOrdered() throws MethodConstraintException {
        TrainingWheels trainingWheels = getSampleTrainingWheels();
        new Rental(
                forOneDay(),
                CUSTOMER_TIMO,
                trainingWheels);
        assertEquals(CUSTOMER_TIMO, trainingWheels.lastlyRentBy());
    }

    @Test
    public void testLastRentBy_moreComplexHistory() throws MethodConstraintException {
        new TestTransaction() {

            TrainingWheels trainingWheels;
            Customer lastOwner;

            @Override
            public void given(Session session) throws MethodConstraintException {
                trainingWheels = getSampleTrainingWheels();
                new Rental(
                        forOneDay(),
                        CUSTOMER_TIMO,
                        trainingWheels);
                new Rental(
                        new Interval(DateTime.now().plusDays(7), DateTime.now().plusDays(10)),
                        CUSTOMER_ELLEN,
                        trainingWheels);
                trainingWheels.discharge(new Discharge(new Interval(
                        DateTime.now().plusDays(11).getMillis(),
                        Long.MAX_VALUE), Discharge.Cause.DAMAGED, "", trainingWheels));
                session.save(trainingWheels);
            }

            @Override
            public void then(Session session) throws MethodConstraintException {
                assertEquals(CUSTOMER_ELLEN, lastOwner);
            }

            @Override
            public void when(Session session) throws MethodConstraintException {
                lastOwner = trainingWheels.lastlyRentBy();
            }

        }.execute();

    }
}
