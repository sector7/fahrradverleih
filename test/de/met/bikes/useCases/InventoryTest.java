package de.met.bikes.useCases;

import static de.met.bikes.useCases.TestUtils.getSampleBikeHerkules;
import static de.met.bikes.useCases.TestUtils.getSampleTrailer;
import static de.met.bikes.useCases.TestUtils.getSampleTrainingWheels;
import static org.junit.Assert.assertEquals;

import java.io.Serializable;

import org.hibernate.Session;
import org.junit.Test;

import de.met.bikes.TestTransaction;
import de.met.bikes.exceptions.MethodConstraintException;
import de.met.bikes.inventory.Item;
import de.met.bikes.inventory.bike.Bike;
import de.met.bikes.inventory.equipment.Trailer;

public class InventoryTest {

    @Test
    public void testAddBikes() throws MethodConstraintException {
        new TestTransaction() {
            private final Bike b = getSampleBikeHerkules();
            private Serializable id;

            @Override
            public void when(final Session session) throws MethodConstraintException {
                this.id = session.save(this.b);
            }

            @Override
            public void then(final Session session) throws MethodConstraintException {
                final Bike bike = session.get(Bike.class, this.id);
                assertEquals(this.b, bike);

            }
        }.execute();
    }

    @Test
    public void testAddEquipment() throws MethodConstraintException {
        new TestTransaction() {
            private final Item i = getSampleTrailer(), j = getSampleTrainingWheels();
            private Serializable iid, jid;

            @Override
            public void when(final Session session) throws MethodConstraintException {
                this.iid = session.save(this.i);
                this.jid = session.save(this.j);

            }

            @Override
            public void then(final Session session) throws MethodConstraintException {
                final Item trailer = session.get(Trailer.class, this.iid);
                assertEquals(this.i, trailer);
                assertEquals(this.j, session.get(Item.class, this.jid));
            }
        }.execute();
    }

}
