package de.met.bikes;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import de.met.bikes.useCases.ContactTest;
import de.met.bikes.useCases.DischargeTest;
import de.met.bikes.useCases.InventoryTest;
import de.met.bikes.useCases.InvoiceTest;
import de.met.bikes.useCases.MaintenanceTest;
import de.met.bikes.useCases.RentalTest;
import de.met.bikes.useCases.ReservationTest;

@RunWith(Suite.class)
@SuiteClasses({
        ContactTest.class,
        DischargeTest.class,
        InventoryTest.class,
        InvoiceTest.class,
        MaintenanceTest.class,
        RentalTest.class,
        ReservationTest.class,
        TestFixtures.class })
public class AllTests {

}
