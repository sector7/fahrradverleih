package de.met.bikes;

import static org.junit.Assert.fail;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.annotations.common.util.impl.LoggerFactory;

import de.met.bikes.exceptions.MethodConstraintException;

/**
 * Helferklasse gegen den Boilerplate-Code. Kümmert sich um das Erstellen und Committen einer
 * Transaktion.
 */
public abstract class TestTransaction {

    /**
     * Führt eine Transaktion aus.
     *
     * @throws MethodConstraintException
     */
    public final void execute() throws MethodConstraintException {
        Transaction tx = null;
        try (Session session = Hibernate.getSession()) {
            tx = session.beginTransaction();
            given(session);
            when(session);
            then(session);
            tx.commit();
        } catch (final HibernateException e) {
            if (tx != null && tx.getStatus().canRollback()) {
                try {
                    tx.rollback();
                } catch (final HibernateException e2) {
                    LoggerFactory.logger(this.getClass()).error((e2.getLocalizedMessage()));
                }
            }
            fail(e.getLocalizedMessage());
        }
    }

    /**
     * Methode, die die Ausgangssituation beschreibt.
     *
     * @param session Session
     */
    public void given(final Session session) throws MethodConstraintException {
    }

    /**
     * Methode in der das erwartete Testergebnis getestet wird.
     *
     * @param session Session
     * @throws MethodConstraintException
     */
    public abstract void then(final Session session) throws MethodConstraintException;

    /**
     * Methode in der die zu testenden Aktionen ausgeführt werden.
     *
     * @param session Session
     * @throws MethodConstraintException
     */
    public abstract void when(final Session session) throws MethodConstraintException;

}
